@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container">
   <ul class="breadcrumb" style="margin-top: 0px;"><li><a href="/">Home</a></li>
       <li class="active">All current lots</li>
   </ul>
</div>

<div class="container" style="min-height: 700px;">
    <div class="top_tab_auction">
        <div class="container">
            <div class="row">
                <ul class="nav nav-tabs mt15">
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <a href="/auction/catalog-list">All current catalogs</a>
                    </li>
                    <li class="active col-md-6 col-sm-12 col-xs-12">
                        <a href="/auction/lot-list">All current lots</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>    
    <br>
    <br>
    
<div class="container">
    <?php        
            $rows = DB::select("
                SELECT lot.`id`,lot.name, lot.`description`, lot.`minimum_bid`, catalog.`datehour` FROM `lot`
          		JOIN `catalog` ON `catalog`.`id` = `lot`.`id_catalog`
          		WHERE catalog.`datehour` >= (SELECT now()) ORDER BY lot.`id` DESC;
            ");

            foreach ($rows as $row)
            {
                $img = DB::select("
                    SELECT `link_to_image` FROM `lot_image` WHERE `id_lot` = '$row->id' LIMIT 1
                ");
                $img = $img[0];
    ?>
    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-item-id="194" data-item-object-type="2">
                <div class="bl_auction_item">
                    <a data-pjax="0" href="/lot/{{$row->id}}" class="bl_img">
                        <img src="/img/{{$img->link_to_image}}" alt="">
                    </a>
                    <i class="fa fa-gavel" aria-hidden="true"></i>
                    <div class="auction_item_content">
                        <a data-pjax="0" href="/lot/{{ $row->id }}">
                            <span class="item_short_text">{{$row->name}}</span>
                        </a>
                        <span class="item_lot">Lot {{$row->id}}</span>
                        <table class="f18 item_table">
                            <tbody>
                                <tr>
                                    <td nowrap="">
                                        <span class="min_price_text">Minimum bid: </span>
                                    </td>
                                    <td>
                                        <span class="min_price_value">€{{$row->minimum_bid}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="item_time"></span>
                        <div style="clear: both"></div>
                        <div class="item_short_text2">{{$row->description}}</div>
                        <a href="/lot/{{$row->id}}" class="item_view_detail">View details</a>
                        <div class="auction_item_fav_btn">
                            
                        <?php
                                $favorite = "";
                                if (Auth::check())
                                {
                                    $userid = Auth::id();
                                    $f = DB::select("
                                        SELECT COUNT(*) as 'nr' FROM `favorite_lots` WHERE `id_lot` = '$row->id' AND `id_user` = '$userid';
                                    "); 
                                    $f = $f[0]->nr;
                                    if ($f > 0)
                                        $favorite = "active";
                                }    
                        ?>
                        <a href="/addtofavoritelots/{{$row->id}}" lot-favorite-id="194" class="auction_item_fav {{$favorite}} set-favorite">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </a>
                            <a href="" lot-calendar-id="194" class="auction_item_calendar" onclick="">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
    <?php
            }
            if (count($rows) == 0)
            {
                echo "No results found!";
            }
    ?>
    
    </div>
</div>
@endsection