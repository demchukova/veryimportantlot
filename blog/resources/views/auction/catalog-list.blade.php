@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container">
   <ul class="breadcrumb" style="margin-top: 0px;"><li><a href="/">Home</a></li>
       <li class="active">All current catalogs</li>
   </ul>
</div>

<?php
    $rows = DB::select(
        "
            SELECT catalog.`id`, `auction_house`.`name` AS 'auction_name', catalog.`name` AS 'catalog_name', catalog.`datehour`, `catalog`.`bg_image`, `catalog`.`image` FROM catalog
            JOIN `auction_house` ON `auction_house`.`id` = `catalog`.`id_auction_house` where datehour >= (SELECT now()) order by catalog.id desc;
        "
    );
?>

<div style="min-height: 600px">
<div class="container" style="min-height: 700px;">
    <div class="top_tab_auction">
        <div class="container">
            <div class="row">
               <ul class="nav nav-tabs mt15">
                    <li class="{{$page[0]}} col-md-6 col-sm-12 col-xs-12">
                        <a href="/auction/catalog-list">All current catalog</a>
                    </li>
                    <li class="{{$page[1]}} col-md-6 col-sm-12 col-xs-12">
                        <a href="/auction/lot-list">All current lots</a>
                    </li>
                </ul>
                 <div class="tab-content">
                   <div class="col-md-12">
                      <div id="catalog-list" class="">
                        <?php
                              foreach($rows as $row)
                              {
                        ?>
                         <div data-key="126">
                             <div class="tab_auction_12_item" data-item-id="126" data-item-object-type="1">
                                 <div class="bl_img"><img src="/img/{{$row->bg_image}}" alt=""></div> <!-- catalog bg image -->
                                 <div class="item_12_content">
                                     <div class="item_12_content_left">
                                         <div class="bl_img1"> <img src="/img/{{$row->image}}" alt=""></div> <!-- catalog image -->
                                     </div>
                                     <div class="item_12_content_right">
                                         <div class="item_12_auction_name">{{ $row->auction_name }}</div>
                                         <div class="item_12_auction_count">
                                        <?php 
                                            $r = DB::select("
                                                select count(*) as 'nr' from lot where id_catalog = '$row->id'
                                            ");
                                             echo $r[0]->nr . " lots of catalogs";
                                        ?></div>
                                         <div class="item_12_auction_text">{{$row->catalog_name}}</div>
                                     </div>
                                     <a href="/auctioncabinet/catalog/show/{{$row->id}}" class="item_12_content"></a>
                                     <div class="item_12_auction_date">
                                         <span class="span_date"><?php echo $row->datehour; ?></span>
                                     </div>
                                 </div>
                             </div>
                         </div>
                    <?php
                          }
                          if(count($rows) == 0)
                          {
                              echo "No results found...";
                          }
                      ?>
                      </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection