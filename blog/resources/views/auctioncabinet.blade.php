@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')

<?php
    if (isset($retu))
    {
        echo "
            <script>
                alertBox('". $retu ."');
            </script>
        ";
        
    }
?>
<div>
    <?php
      
        $userid = Auth::id();
    ?>

<div style="min-height: 600px">
    <?php
      
        $userid = Auth::id();
        $userdata = DB::table('users')->where('id', '=', Auth::id())->first();
        $auctiondata = DB::table('auction_house')->where('id_user', '=', Auth::id())->first();
        $catalogs = DB::table('catalog')->where('id_auction_house', '=',$auctiondata->id)->get();
    ?>
    @include ('layouts.auctioncabinetnav')
    <br>
    <br>
    
    <div class="container" style="">
        <div class="row bl_user_setings new_form mt48 mb105">

            <form action="/updateAuctionHouse" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="_csrf-frontend" value="MDh2VTlCZ2NBCABidHQoBl5vLy1RNCE0X0IePV84Xxdgcz8kfChTCA==">
                <div class="col-md-6">
                    <div class="user_setings_title">Data of the auction house</div>
                    <br>
                    <div class="form-horizontal mt10">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-name">
                                    <div style="margin-top:8px;">{{ $userdata->email }}</div>
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Name:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-name">

                                    <input type="text" id="user-name" class="form-control" name="User[name]" value="{{$auctiondata->name}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Contact person:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-contact-person">

                                    <input type="text" id="user-contact-person" class="form-control" name="User[contact_person]" value="{{$auctiondata->contact_person}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Address:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-address required">

                                    <input type="text" id="user-address" class="form-control" name="User[address]" value="{{$auctiondata->address}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Postal code:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-zip_code required">

                                    <input type="text" id="user-zip_code" class="form-control" name="User[zip_code]" value="{{$auctiondata->postal_code}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">City:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-city required">

                                    <input type="text" id="user-city" class="form-control" name="User[city]" value="{{$auctiondata->city}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Country:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-country_id required">

                                    <input type="text" id="user-country" class="form-control" name="User[country]" value="{{$auctiondata->country}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Phone number:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-phone-number required">

                                    <input type="text" id="user-phone-number" class="form-control" name="User[phone_number]" value="{{$auctiondata->phone_number}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Web site:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-website required">

                                    <input type="text" id="user-website" class="form-control" name="User[website]" value="{{$auctiondata->website}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="user_setings_title">Change password</div>
                    <br>
                    <div class="form-horizontal mt10">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Old password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-old_password">

                                    <input type="password" id="user-old_password" class="form-control" name="User[old_password]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">New password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-password">

                                    <input type="password" id="user-password" class="form-control" name="User[password]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Repeat password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-password_repeat">

                                    <input type="password" id="user-password_repeat" class="form-control" name="User[password_repeat]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt50">
                    <button type="submit" class="btn btn-default btn-block dark">Save</button>
                    <br>
                    <br>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection