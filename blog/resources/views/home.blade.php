@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<?php
    if (isset($reteredUser))
    {
        echo "
            <script>
                alertBox('Hi " . $reteredUser . ", You have been registrated successfully, please verify your email');
            </script>
        ";
    }
        
?>
<div class="banner">
    <div class="container" style="position: relative;">
        <div class="bl_top_img">
            <div class="top_img_left">
                <div class="top_img_logo"> <img src="/img/top_img_logo2.png"> </div>
                <div class="top_imgcontent">
                    <a href="/auction/catalog-list"><span>@lang('msg.auction_catalogs')</span></a>
                    <a href="/auction/lot-list"><span>@lang('msg.all_lots')</span></a>
                    <a href="/gallery/gallery-list"><span>@lang('msg.all_galleries')</span></a>
                </div>
            </div>
            <div class="betta"></div>
            <div class="top_img_div parallaximg"> </div>
        </div>
    </div>
</div>

<div class="container text-center">
        <h1>        
            <img src="/img/logo_header_black.svg" width="100"/>
        </h1>
        <p></p><p>"Very Important Lot" ist eine benutzerfreundliche Webseite mit besonderen Funktionen.</p>

<p>Im Kern von "Very Important Lot"&nbsp;liegen viele neue Ideen und das durchdachte Design.&nbsp;Wir wollen Auktionshäuser und Antiquitätengalerien in einem gemeinsamen Netzwerk vereinen, eine schnelle Suche seltener Sammlungsobjekten ermöglichen und zahlungsfähige Kunden aus Rußland, Kasachstan und weiteren im Osten liegenden Länder als Neukunden anbieten.</p>

<p>------<strong>"Very Important Lot" wird in russischsprachigen Regionen stark gemacht</strong>.------</p>

<p>Die Grundfunktionen von&nbsp;"Very Important Lot" nutzen Sie kostenlos, die Rechte auf Tarif "Premium"&nbsp;und Tarif "Premium Pro" sind ebenfalls kostenlos solange der BETA-TEST nicht abgeschlossen ist. Bei Tarifen "Premium" und "Premium Pro" werden später monatlichen pauschale Gebühren fällig&nbsp;. Wir erheben <strong>keine Provision</strong> von Ihren verkauften Produkten. Tarife können<strong> jederzeit mit sofortigen Wirkung gekündigt werden</strong>. Sollte der Trafik von&nbsp;"Very Important Lot" den gewünschten&nbsp;Wert in Jahr 2017 nicht erreichen, werden Tarife "Premium" und "Premium Pro" weiterhin kostenlos angeboten.</p>

<p>&nbsp;</p>

<p>------ Bei&nbsp;"Very Important Lot" können Artikel erst&nbsp;<strong>ab 1000€</strong> Angeboten werden! ------</p>

<p>------ Es steht&nbsp;Ihnen&nbsp;4 Währungen <strong>€&nbsp;£&nbsp;$&nbsp;₽&nbsp;</strong>und 4 Sprachen <strong>DE EN FR RU&nbsp;</strong>zur Verfügung&nbsp;<strong>&nbsp;</strong>------</p>

<p>------ Sie können unsere Kunden auf&nbsp;<strong>Ihre eigene Webseite </strong>weiterleiten&nbsp;------</p>

<p>&nbsp;</p>

<p>Der Beta - Test&nbsp;ist vom <strong>15.01.2017</strong>&nbsp;bis zum <strong>31.03.2017 geplant</strong>. Ihre Vorschläge zur Verbesserung der Benutzerfreundlichkeit "Very Important Lot" können&nbsp;Sie als Kommentare auf unseren Seiten in den sozialen Netzwerken <a href="https://www.facebook.com/veryimportantlot/">Facebook</a>, <a href="https://twitter.com/VeryLot">Twitter</a> und <a href="https://vk.com/veryimportantlot">VKontakte</a>&nbsp;lassen können</p>

<p>Wir werden Ihre Vorschläge berücksichtigen und Ihnen eine&nbsp;bequeme und qualitativ hochwertige Dienstleistungen aur "Very Important Lot" bieten.</p>

<p>--------------</p>

<p>Wir bedanken uns bei Ihnen für die Teilnahme</p>

<p>Administration Very Important Lot</p>

<p>&nbsp;</p>
<p></p>
    </div>
    
@endsection
