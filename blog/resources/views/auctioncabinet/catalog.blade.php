
@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
@include('layouts.auctioncabinetnav')

<script type="text/javascript">
    function deleteCatalog(catid)
    {
        document.getElementById(catid).style.display = 'none';
    }
</script>
<div class="container" style="min-height: 700px;">
        <?php 
            $auctiondata = DB::table('auction_house')->where('id_user', Auth::id())->first();
            $catalogs = DB::table('catalog')->where('id_auction_house', '=',$auctiondata->id)->orderBy('id', 'desc')->get();
        ?>
        <div class="col-sm-10"></div>
         <div class="col-sm-2">
             <div style="position:absolute;width:100%;top:50%;">
             <a href="/auctioncabinet/catalog/create"><button type="button" class="btn btn-success" style="width: 100%">+Add catalog</button></a>
             </div>
        </div>
        <br><br>
     @foreach ($catalogs as $catalog)
    <div id="catalog{{ $catalog->id }}">
     <div class="col-sm-10" >
         <div class="tab_auction_12_item">
             <div class="bl_img"><img src="/img/{{$catalog->bg_image}}" alt=""></div>
                  <div class="item_12_content">
                 <div class="item_12_content_left">
                    <div class="bl_img1"> <img src="/img/{{$catalog->image}}" alt="" style="height:100%"></div>
                </div>
         <div class="item_12_content_right">
            <div class="item_12_auction_name">Catalog {{ $catalog->name }}</div>
                <div class="item_12_auction_count">Lots: <?php echo DB::table('lot')->where('id_catalog', $catalog->id)->count(); ?></div>
                <div class="item_12_auction_text">{{$auctiondata->name}}</div>
                <div class="item_12_auction_date"><span class="span_date"> Date of auction {{$catalog->datehour }}</span></div>
             </div>
         </div>
         </div>
     </div>
    <div class="col-sm-2" style="height:300px;">
        <div style="position:absolute;width:100%;top:50%;transform: translate(0, -50%);">
         <a href="/auctioncabinet/catalog/show/{{$catalog->id}}"><button type="button" class="btn btn-primary btn-block">View catalog</button></a><br>
         <a href="/auctioncabinet/catalog/editcatalog/{{$catalog->id}}"><button type="button" class="btn btn-success btn-block" >Edit</button></a><br>
         <!--<a href="/auctioncabinet/catalog/deletecatalog/{{$catalog->id}}"><button type="button" class="btn btn-danger btn-block" >Delete</button></a>-->
        </div>
    </div>
        <br>

    </div>
     @endforeach
</div>
@endsection