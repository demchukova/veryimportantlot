@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
@include('layouts.auctioncabinetnav')

<div class="container" style="min-height: 700px;">
    <h1>
        <?php 
            $auctiondata = DB::table('auction_house')->where('id_user', Auth::id())->first();
            $catalogs = DB::table('catalog')->where('id_auction_house', '=', $auctiondata->id)->get();
        ?>
        
        <ul>
            @foreach ($catalogs as $catalog)
                        <?php 
                            $lots = DB::table('lot')->where('id_catalog', $catalog->id)->get();
                        ?>
                            @foreach ($lots as $lot) 
                                <?php $rates = DB::table('rates')->where('id_lot', $lot->id)->get(); 
                                ?>
                                @if ($rates->count())
                                <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-item-id="194" data-item-object-type="2">
                                    <div class="bl_auction_item">
                                        <i class="fa fa-gavel" aria-hidden="true"></i>
                                        <div class="auction_item_content">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" style="background-image: linear-gradient(to bottom,#000000 0,#505050 100%)">
                                                <span class="item_short_text" style="color: #fff">Lot Number {{$lot->id}}: {{$lot->name}}</span>
                                            </div>
                                    @foreach ($rates as $rate)
                                        <?php $name = DB::table('all_users')->where('id', $rate->id_user)->first();
                                            ?>
                                            <ul class="list-group">
                                                <li class="list-group-item"><span class="min_price_value">Bid: €{{$rate->value}}</span></li>
                                                <li class="list-group-item"> <span class="min_price_value">User: {{ $name->first_name }} {{ $name->last_name }}</span></li>
                                            </ul>
                                    @endforeach
                                </div>
                                 </div>
                                 </div>
                                </div>
                                @endif
                                <hr>
                            @endforeach
                        </li>
            @endforeach
        </ul>
    </h1>
</div>

@endsection