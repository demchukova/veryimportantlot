<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<body>
<body>
<div class="mail-wrapper" style="background-color: #ecf0f1; box-sizing: border-box; color: #33414a; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.42857143; padding: 60px 15px; width: 100%;">
    <table cellpadding="0" cellspacing="0" class="table-center header-table" style="background-color: #000000; border: 0 none; border-collapse: collapse; box-sizing: border-box; margin: 0 auto; max-width: 600px; min-width: 600px; position: relative; width: 100%;">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box; padding: 0 30px;">
                <ul style="box-sizing: border-box; list-style: none; margin: 0; padding: 0;">
                    <li style="box-sizing: border-box; display: inline-block; font-size: 12px; margin-right: 20px; padding: 5px 0; text-transform: uppercase;">
                        <a href="http://blo-demchukova.c9users.io/" target="_blank" style="box-sizing: border-box; color: #fff; text-decoration: none;"><img src="http://blo-demchukova.c9users.io/img/logo_header.svg" height=25px; width=25px; alt="VIL" style="box-sizing: border-box; margin-right: 10px; vertical-align: middle;">Home</a>
                    </li>
                    <li style="box-sizing: border-box; display: inline-block; font-size: 12px; margin-right: 20px; padding: 5px 0; text-transform: uppercase;">
                        <a href="http://blo-demchukova.c9users.io/" target="_blank" style="box-sizing: border-box; color: #fff; text-decoration: none;">Auctions</a>
                    </li>
                    <li style="box-sizing: border-box; display: inline-block; font-size: 12px; margin-right: 20px; padding: 5px 0; text-transform: uppercase;">
                        <a href="http://blo-demchukova.c9users.io/" target="_blank" style="box-sizing: border-box; color: #fff; text-decoration: none;">Galleries</a>
                    </li>
                </ul>
            </td>
        </tr>
        </tbody>
    </table>

    <table cellpadding="0" cellspacing="0" class="table-center main-table" style="background-color: #ffffff; border: 0 none; border-collapse: collapse; box-sizing: border-box; margin: 0 auto; max-width: 600px; min-width: 600px; position: relative; width: 100%;">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td class="logo" style="box-sizing: border-box; padding: 0 30px;">
                <img src="http://blo-demchukova.c9users.io/img/top_img_logo2.png" alt="Very Important Lot" style="box-sizing: border-box; max-width: 100%;">
            </td>
            <td class="hammer-bg" style="background-image: url(http://blo-demchukova.c9users.io/img/hammer-bg.jpg); background-position: center right; background-repeat: no-repeat; box-sizing: border-box; height: 130px; padding: 0 30px; width: 50%;">
                <table cellpadding="0" cellspacing="0" class="stamp" style="background-position: center left; background-repeat: no-repeat; box-sizing: border-box; height: 100%; width: 100%;">
                    <tbody style="box-sizing: border-box;">
                    <tr style="box-sizing: border-box;">
                        <td style="box-sizing: border-box;">
                            &nbsp;
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr style="box-sizing: border-box;">
            <td colspan="2" class="devider" style="box-sizing: border-box; padding: 0 30px;">
                <div style="border-top: 1px solid #000000; box-sizing: border-box; height: 1px; width: 100%;"></div>
            </td>
        </tr>

        <tr style="box-sizing: border-box;">
            <td colspan="2" class="body-text" style="box-sizing: border-box; padding: 0 30px;">
                <!--[CDATA[YII-BLOCK-BODY-BEGIN]]-->                <div class="password-reset">

    <p>
        Dear partner,<br>
        Many thanks for completing the online registration form.
        Please wait until you have received confirmation that your registration has been accepted.
       </p>
</div>
                <!--[CDATA[YII-BLOCK-BODY-END]]-->            </td>
        </tr>




        <tr style="box-sizing: border-box;">
            <td colspan="2" class="footer-text" style="box-sizing: border-box; font-size: 12px; padding: 0 30px; padding-bottom: 20px;">
                <p style="box-sizing: border-box;">
                    Administration Very Important Lot                </p>
            </td>
        </tr>
        </tbody>
    </table>


    <table cellpadding="0" cellspacing="0" class="table-center footer-table" style="background-color: transparent; border: 0 none; border-collapse: collapse; box-sizing: border-box; color: #7d7d7d; font-size: 12px; margin: 0 auto; max-width: 600px; min-width: 600px; position: relative; width: 100%;">
        <tbody style="box-sizing: border-box;">
        <tr style="border-bottom: 1px solid #bebebe; box-sizing: border-box;">
            <td colspan="3" style="box-sizing: border-box; padding: 10px 0;">
                <ul style="box-sizing: border-box; list-style: none; margin: 0; padding: 0;">
                    <li style="box-sizing: border-box; display: inline-block; margin-right: 20px;">
                        <a href="http://blo-demchukova.c9users.io/wiki/8" style="box-sizing: border-box; color: #7d7d7d; text-decoration: none;">
                            Terms of use                        </a>
                    </li>
                    <li style="box-sizing: border-box; display: inline-block; margin-right: 20px;">
                        <a href="http://blo-demchukova.c9users.io/wiki/10" style="box-sizing: border-box; color: #7d7d7d; text-decoration: none;">
                            Privacy policy                        </a>
                    </li>
                    <li style="box-sizing: border-box; display: inline-block; margin-right: 20px;">
                        <a href="http://blo-demchukova.c9users.io/wiki/9" style="box-sizing: border-box; color: #7d7d7d; text-decoration: none;">
                            Imprint                        </a>
                    </li>
                </ul>
            </td>
        </tr>

        <tr style="border-bottom: 1px solid #bebebe; box-sizing: border-box;">
            <td style="box-sizing: border-box; padding: 10px 0; vertical-align: top">
                Business owner:<br style="box-sizing: border-box;"> Svetlana Demchukova            </td>
            <td style="box-sizing: border-box; padding: 10px 0;">
                Contact:<br style="box-sizing: border-box;"> Phone: +49 (0)4298 6996454
                <br style="box-sizing: border-box;">
                Email: info@veryimportantlot.com
            </td>
            <td style="box-sizing: border-box; padding: 10px 0;">
                Address:<br style="box-sizing: border-box;">
                An der Wert 21<br style="box-sizing: border-box;"> 28865 Lilienthal/ DE
            </td>
        </tr>
        <tr style="border-bottom: 0 none; box-sizing: border-box;">
            <td colspan="3" style="box-sizing: border-box; padding: 10px 0;">
                © Very Important Lot, 2017
            </td>
        </tr>
        </tbody>
    </table>

</div>
 
</body>