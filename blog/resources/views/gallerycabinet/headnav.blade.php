    @include ('gallery.add')
    <?php
        $userid = Auth::user()->id;
        $gallid = DB::table('galleries')->select('id')->where('id_user', '=', $userid)->get();
    ?>
    <div class="lk_nav_menu" style="border-bottom:1px solid #bababa;">
        <div class="container">
            <ul class="">
                <li>
                    <a href="/gallery/{{$gallid[0]->id}}">My gallery</a>
                </li>
                <li>
                    <a href="#" type="button" class="" data-toggle="modal" data-target="#myModal">Add Item</a>
                </li>
                <li>
                    <a href="#" type="button" class="" data-toggle="modal" data-target="#myModal2">Change Image</a>
                </li>
                <li class="lk_menu_user_setings">
                    <a href="/usercabinet" class=""><i class="fa fa-cogs" aria-hidden="true"></i> 
                <?php 
                        $userdata = DB::table('users')->where('id', '=', Auth::id())->first();
                        echo $userdata->email; 
                ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>