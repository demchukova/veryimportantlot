@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')

<?php
    if (isset($retu))
    {
        echo "
            <script>
                alertBox('". $retu ."');
            </script>
        ";
        
    }
    $userdata = DB::table('galleries')->where('id_user', '=', Auth::id())->first();
    $email = DB::table('users')->where('id', '=', Auth::id())->first();
?>
    
    @include('gallerycabinet.headnav')

    <br>
    <br>
    
    <div class="container">
        <div class="row bl_user_setings new_form mt48 mb105">

            <form action="/savePersonalInfo" method="post">
                {{ csrf_field() }}
                <div class="col-md-6">
                    <div class="user_setings_title">Data of the user</div>
                    <br>
                    <div class="form-horizontal mt10">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-firstname">
                                    <div style="margin-top:8px;">{{$email->email}}</div>
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Name:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-firstname">
                                    <input type="text" id="user-firstname" class="form-control" name="Gallery[name]" value="{{$userdata->name}}" autocomplete="off">
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Contact person:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-lastname">

                                    <input type="text" id="user-lastname" class="form-control" name="Gallery[contact_person]" value="{{$userdata->contact_person}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Address:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-address required">

                                    <input type="text" id="user-address" class="form-control" name="Gallery[address]" value="{{$userdata->address}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Postal code:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-zip_code required">

                                    <input type="text" id="user-zip_code" class="form-control" name="Gallery[postal_code]" value="{{$userdata->postal_code}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">City:</label>
                            <div class="col-md-6">
                                <div class="form-group field-user-city required">

                                    <input type="text" id="user-city" class="form-control" name="Gallery[city]" value="{{$userdata->city}}" autocomplete="off">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Country:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-country_id required">

                                    <input type="text" id="user-country" class="form-control" name="Gallery[country]" value="{{$userdata->country}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Phone number:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-country_id required">

                                    <input type="text" id="user-country" class="form-control" name="Gallery[phone_number]" value="{{$userdata->phone_number}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Website:</label>
                            <div class="col-md-6">

                                <div class="form-group field-user-country_id required">

                                    <input type="text" id="user-country" class="form-control" name="Gallery[website]" value="{{$userdata->website}}">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="user_setings_title">Change password</div>
                    <br>
                    <div class="form-horizontal mt10">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Old password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-old_password">

                                    <input type="password" id="user-old_password" class="form-control" name="Gallery[old_password]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">New password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-password">

                                    <input type="password" id="user-password" class="form-control" name="Gallery[password]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Repeat password:</label>
                            <div class="col-md-8">
                                <div class="form-group field-user-password_repeat">

                                    <input type="password" id="user-password_repeat" class="form-control" name="Gallery[password_repeat]">

                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mt50">
                    <button type="submit" class="btn btn-default btn-block dark">Save</button>
                    <br>
                    <br>
                </div>

            </form>

        </div>
    </div>

@endsection