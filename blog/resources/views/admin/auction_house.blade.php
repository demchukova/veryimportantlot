@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')

<div class="container"  style="min-height: 700px;">
        <a href="/admin"><button class="btn btn-default btn_admin" type="submit">Back</button></a>
        <h1 align="center">        
            <img src="/img/logo_header_black.svg" width="100"/>
            Auction House!
        </h1>
        <div class="panel panel-default">
          <div class="panel-body">
              <?php
                $auctionHouse = DB::select("select * from auction_house where id_user = any(select id from users where verified = 0 and role = 3 )"); 
              ?>
            @foreach ($auctionHouse as $achouse)
            <div class="panel1">
            <div class="panel panel-default">
                <div class="panel-heading panel_admin">
                    <h3 class="panel-title"><strong>{{ $achouse->name }}</strong></h3> 
                        <div class="panel-body">
                          <ul class="list-group">
                            <li class="list-group-item">{{ $achouse->website }}</li>
                            <li class="list-group-item">{{ $achouse->contact_person }}</li>
                            <li class="list-group-item">{{ $achouse->phone_number }}</li>
                         </ul>
                         </div>
                    </li><a href="/admin/auction_house/{{$achouse->id_user}}"><button type="button" class="btn btn-success" >Activate</button></a>
                </div>
            </div>
            </div>
            @endforeach
            </div>
        </div>
    </div>
            
@endsection