@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')

<div class="container" style="min-height: 700px;">
     <a href="/admin"><button class="btn btn-default btn_admin" type="submit">Back</button></a>
        <h1 align="center">        
            <img src="/img/logo_header_black.svg" width="100"/>
            Galleries!
        </h1>
        <div class="panel panel-default">
          <div class="panel-body">
                <?php
                $galleries =  DB::select("select * from galleries where id_user = any(select id from users where verified = 0 and role = 2 )"); 
              ?>
        

            @foreach ($galleries as $gallery)
            <div class="panel panel-default">
                <div class="panel-heading panel_admin">
                    <h3 class="panel-title"><strong>{{ $gallery->name }}</strong></h3> 
                        <div class="panel-body">
                          <ul class="list-group">
                             <li class="list-group-item">{{ $gallery->website }}</li>
                              <li class="list-group-item">{{ $gallery->contact_person }}</li>
                              <li class="list-group-item">{{ $gallery->phone_number }}</li>
                          </ul>
                         </div>
                    <a href="/admin/galleries/{{$gallery->id_user}}"><button type="button" class="btn btn-success">Activate</button></a>
                </div>
            </div>
            @endforeach
            </div>
        </div>
</div>
        
@endsection

