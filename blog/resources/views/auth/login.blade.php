@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
<!-- LOGIN -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    function _(elem)
    {
        return document.getElementById(elem);
    }

    function checkEmail(email)
    {
        var xhttp = new XMLHttpRequest();
        
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) 
            {
                if (xhttp.responseText.localeCompare('[{"verified":1}]') == 0)
                    document.getElementById('logForm').submit();
                else
                    alertBox("Hi, " + email + " is not verified! ");
            }
        }
        xhttp.open("get", "checkEmail/"+email, true);
        xhttp.send();
    }
</script>
<div>
    <div class="onBottom"></div>
    <div class="container" style="min-height: 700px;">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <img src="/img/logo_header_black.svg" width="70" />
                        <h3>@lang('msg.log_in')</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" id="logForm" role="form" method="POST" name="loginForm" action="login">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail </label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">@lang('msg.password')</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label class="switch">
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <div class="slider round"></div>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@lang('msg.remember')
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="button" class="btn btn-default dark" id="submitBTN" 
                                            onclick="checkEmail(loginForm.email.value)">
                                        Login
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        @lang('msg.forgot')
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
