@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
<?php
     function isUser(int $id)
    {
        $is_user =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_user[0]->role == 1)
            return 1;
        else return 0;
    }
    $favorite = "-o";
    if (Auth::check() && isUser(Auth::id()))
    {
        $userid = DB::table('all_users')->select('id')->where('id_user', '=', Auth::id())->first();
        $userid = $userid->id;
        $f = DB::select("
            SELECT COUNT(*) as 'nr' FROM `favorite_lots` WHERE `id_lot` = '$lotid' AND `id_user` = '$userid';
        "); 
        $f = $f[0]->nr;
        if ($f > 0)
            $favorite = "";
    }

    $imgs = DB::select("
        SELECT `lot_image`.`link_to_image` FROM `lot_image` WHERE `lot_image`.`id_lot` = '$lotid'
    ");
    $lot = DB::select("
    SELECT lot.* , catalog.`datehour` FROM `lot` 
    JOIN catalog ON catalog.`id` = lot.`id_catalog`
    WHERE `lot`.`id` = '$lotid';
    ");
    $lot = $lot[0];
    $mybid = $lot->minimum_bid;
    if (Auth::check() && isUser(Auth::id()))
    {
         $user = DB::table('all_users')->where('id_user', Auth::id())->first();
        $userid = $user->id;
        $mybid = DB::select("
            select value from rates where id_lot = '$lotid' and  id_user = '$userid' order by value desc; 
        ");
        if (!empty($mybid))
            $mybid= $mybid[0]->value;
        else
            $mybid = $lot->minimum_bid;
    }
?>
<div class="container">
   <ul class="breadcrumb" style="margin-top: 0px;"><li><a href="/">Home</a></li>
       <li class="active">View lot</li>
   </ul>
</div>
<div class="container bl_prod_page bl_galleries_product_page">
<div class="row">
    <div class="col-md-6">
        <div id="bl_prod_carousel" class="carousel slide" data-ride="carousel">
            <div class="prod_carousel_top">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                        
                    <?php
                        $active = "active";
                        foreach($imgs as $img)
                        {
                    ?>

                        <div class="item <?php echo $active; $active = ""; ?>">
                            <a class="bl_img fancybox" target="_blank" href="">
                            <img src="/img/{{$img->link_to_image}}" />
                            </a>
                        </div>
                    
                    <?php
                        }
                    ?>
                    
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#bl_prod_carousel" role="button" data-slide="prev">
                    <span><i class="fa fa-angle-left" aria-hidden="true"></i></span>
                </a>
                <a class="right carousel-control" href="#bl_prod_carousel" role="button" data-slide="next">
                    <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                </a>
            </div>
            <!-- Indicators -->
            <div class="row">
                <ol class="carousel-indicators">
                     <?php
                        $active = "active";
                        $i = 0;
                        foreach($imgs as $img)
                        {
                     ?>
                    
                        <li data-target="#bl_prod_carousel" data-slide-to="{{$i++}}" class="
                        <?php echo $active; $active = ""; ?>">
                            <div class="bl_img"><img src="/img/{{$img->link_to_image}}" /></div>
                        </li>
                    
                    <?php
                        }
                    ?>
                    
                </ol>
            </div>
        </div>
    </div>
    <!-- -->
    <div class="col-md-6">
        <div class="d_table w100p">
            <div class="col-md-6">
                <div class="d_table f_left">
                    <a href="/auctioncabinet/catalog/show/{{$lot->id_catalog}}" class="bl_i_fav_h">
                        <span>View catalog</span>
                        <i class="fa fa-book" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="d_table f_right txt_right">
                    <a href="/addtofavoritelots/{{$lotid}}" class="bl_i_fav show-login">
                        <span>Favorites</span>
                        <i class="fa fa-star{{$favorite}}" aria-hidden="true"></i>
                    </a>

                    <a href="javascript:;" lot-calendar-id="194" class=" bl_i_calendar" onclick="">
                        <span>Calender</span>
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                        <i class="fa fa-calendar-o" aria-hidden="true"><span>5</span></i>
                    </a>
                    <a href="javascript:;" class="bl_i_share" data-toggle="modal" data-target="#share_modal">
                        <span>Share</span>
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="col-md-6 c_gray">
            <span class="d_table f16">Estimate value:</span><br>
            <span class="d_table f30">€ {{$lot->minimum_bid}}</span>
        </div>
        <div class="col-md-6 c_gray">
            <span class="d_table f16">Auction date:</span><br>
            <span class="d_table" style="font-size:18pt;">{{$lot->datehour}}</span>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <form action="/lot/makebid" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="lotid" value="{{ $lotid }}">
            <input type="number" name="bid" class="bitNR col-md-6" min='{{$lot->minimum_bid}}' value="{{ $mybid }}"/>
            <button type="submit" class="bitNRBTN col-md-6">BID NOW</button>
        </form>
        <br>
        <br>
        <br>
        <br>
        <button class="bitNRBTN col-md-12">BID BY PHONE</button>
    </div>
    <div class="col-md-12">
        <h3>Descripton</h3>
        <p>{{$lot->description}}</p>
    </div>
</div>
</div>
@endsection