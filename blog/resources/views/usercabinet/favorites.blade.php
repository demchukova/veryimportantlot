@extends('layouts.app') 
@extends('layouts.nav')
@extends('layouts.footer') 
@section('content') 

@include('layouts.usercabinetnav')

<?php
    $userdata = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
    $userid = $userdata->id;
    $favlotsnr = [0 => ''];
    $favcatalognr = [0 => ''];
    $favgalleriesnr = [0 => ''];
    $favgalleriesitemsnr = [0 => ''];
    $favlotsnr = DB::select("SELECT count(*) as 'nr' FROM `favorite_lots` where `id_user` = '$userid'");
    $favcatalognr = DB::select("SELECT count(*) as 'nr' FROM `favorite_catalogs` where `id_user` = '$userid'");
    $favgalleriesnr = DB::select("SELECT count(*) as 'nr' FROM `favorite_galleries` where `id_user` = '$userid'");
    $favgalleriesitemsnr = DB::select("SELECT count(*) as 'nr' FROM `favorite_gallery_item` where `id_user` = '$userid'");
?>
    <div class="container" style="min-height: 700px;">
        <br>
        <br>
        <div class="row">
            <ul class="nav navCenter">
                <li class="col-md-3 {{$fav[0]}} ">
                    <a href="/usercabinet/favorites">
            Auction lots <span class="drop_count">{{$favlotsnr[0]->nr}}</span>
        </a>
                </li>
                <li class="col-md-3 {{$fav[1]}}">
                    <a href="/usercabinet/favoritecatalogs" aria-controls="auction_cat">
            Auction catalogs            <span class="drop_count">{{$favcatalognr[0]->nr}}</span>
        </a>
                </li>
                <li class="col-md-3 {{$fav[2]}}">
                    <a href="/usercabinet/favoritegalleryitems" aria-controls="gallery_expo">
            Galleries items            <span class="drop_count">{{$favgalleriesitemsnr[0]->nr}}</span>
        </a>
                </li>
                <li class="col-md-3 {{$fav[3]}}">
                    <a href="/usercabinet/favoritegalleries" aria-controls="gallery" role="tab">
            Galleries            <span class="drop_count">{{$favgalleriesnr[0]->nr}}</span>
        </a>
                </li>
            </ul>

            <br>
            <?php
            
            function page($f)
            {
                if (strlen($f[0]) > 0)
                    return (1);
                else if (strlen($f[1]) > 0)
                    return (2);
                else if (strlen($f[2]) > 0)
                    return (3);
                else if (strlen($f[3]) > 0)
                    return (4);
                return (1);
            }
            
            $dellink = "favorites";
            switch (page($fav))
            {
                case 1: /*auction lots */ 
                    $rows = DB::select("
                        SELECT `favorite_lots`.id, lot.id as 'lotid', lot.name, lot.`description`, lot.`minimum_bid`, catalog.name AS 'from_catalog', catalog.conditions, all_users.`first_name`, all_users.`last_name` FROM `favorite_lots`
                        JOIN lot ON lot.id = favorite_lots.`id_lot`
                        JOIN all_users ON `all_users`.`id` = favorite_lots.`id_user`
                        JOIN catalog ON catalog.`id` = lot.`id_catalog`
                        WHERE `favorite_lots`.`id_user` = '$userid'
                    ");
                $dellink = "favorites";
                if(!empty($rows)) 
                    foreach ($rows as $row) 
                    {
                    $img = DB::select("
                        SELECT `link_to_image` FROM `lot_image` WHERE `id_lot` = '$row->lotid' LIMIT 1
                    ");
                    $img = $img[0];
                ?>
                <div class="bit_body col-md-12 ">
                    <div class="borderRight col-md-6">
                        <span class="lot_photo"><img src="/img/{{$img->link_to_image}}"/></span>
                        <p class="pheading">Lot: {{$row->name}}</p>
                        <i class="pheading">Catalog: {{$row->from_catalog}} </i>
                        <p class="pheading">minimal bid: {{$row->minimum_bid}} $</p>
                    </div>
                    <div class="borderLeft col-md-6">
                        <p class="pheading">conditions: {{$row->conditions}}</p>
                        <p class="pheading">description: {{$row->description}}</p>
                        <p class="pheading">user name: {{$row->first_name . " " . $row->last_name}}</p>
                    </div>
                    <a href="/usercabinet/{{$dellink}}/delete/{{$row->id}}" class="deleteBit">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>

                <?php
                    }
                    if(count($rows) == 0)
                    {
                        echo "No results found";
                    }
                break;
            case 2: /*auction catalogs */ 
                    $rows = DB::select("
                        SELECT `favorite_catalogs`.id, catalog.`name`, catalog.`conditions`, catalog.`exibition_start`,  catalog.`exibition_end`, `auction_house`.`name` AS 'auction_house', `auction_house`.`website`, catalog.image FROM `favorite_catalogs`
                        JOIN catalog ON catalog.`id` = `favorite_catalogs`.`id_catalog`
                        JOIN `auction_house` ON `auction_house`.`id` = catalog.`id_auction_house`
                        WHERE `favorite_catalogs`.`id_user` = '$userid'
                    ");
                $dellink = "favoritescatalogs";
                if(!empty($rows)) 
                    foreach ($rows as $row) 
                    {
                ?>
                <div class="bit_body col-md-12 ">
                    <div class="borderRight col-md-6">
                        <span class="lot_photo"><img src="/img/{{$row->image}}"/></span>
                        <p class="pheading">Catalog: {{$row->name}}</p>
                        <i class="pheading">From Auction house: {{$row->auction_house}} </i>
                        <p class="pheading">conditions: {{$row->conditions}}</p>
                    </div>
                    <div class="borderLeft col-md-6">
                        <p class="pheading">Start: {{$row->exibition_start}}</p>
                        <p class="pheading">End: {{$row->exibition_end}}</p>
                        <p class="pheading">Website: {{$row->website}}</p>
                    </div>
                    <a href="/usercabinet/{{$dellink}}/delete/{{$row->id}}" class="deleteBit">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>

                <?php
                    }
                    if(count($rows) == 0)
                    {
                        echo "No results found";
                    }
                break;
                    
            case 3: /* galleries items */ 
                    $rows = DB::select("
                        SELECT `favorite_gallery_item`.id, `gallery_item`.name, `gallery_item`.`description`, `gallery_item`.`price`, galleries.`name` AS 'from_galley', `gallery_item_image`.`link_to_image` FROM `favorite_gallery_item`
                        JOIN `gallery_item` ON `gallery_item`.`id` = `favorite_gallery_item`.`id_gallery_item`
                        JOIN `galleries` ON `galleries`.`id` = `gallery_item`.`id_gallery`
                        JOIN `gallery_item_image` ON `gallery_item_image`.`id_gallery_item` = `gallery_item`.`id`
                        WHERE `favorite_gallery_item`.`id_user` = '$userid'
                    ");
                $dellink = "favoritegalleryitems";
                if(!empty($rows)) 
                    foreach ($rows as $row) 
                    {
                ?>
                <div class="bit_body col-md-12 ">
                    <div class="borderRight col-md-6">
                        <span class="lot_photo"><img src="/img/{{$row->link_to_image}}"/></span>
                        <p class="pheading">Name: {{$row->name}}</p>
                        <i class="pheading">From gallery: {{$row->from_galley}} </i>
                        <p class="pheading">Price: {{$row->price}}$</p>
                    </div>
                    <div class="borderLeft col-md-6">
                        <p class="pheading">Description: {{$row->description}}</p>
                    </div>
                    <a href="/usercabinet/{{$dellink}}/delete/{{$row->id}}" class="deleteBit">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>

                <?php
                    }
                    if(count($rows) == 0)
                    {
                        echo "No results found";
                    }
                break;
            case 4: /* galleries */ 
                    $rows = DB::select("
                        SELECT `favorite_galleries`.`id`, galleries.name, `galleries`.`website`, `galleries`.`contact_person`, `galleries`.`country`, `galleries`.`city`, `galleries`.`address` FROM `favorite_galleries` 
                        JOIN galleries ON `galleries`.`id` = `favorite_galleries`.`id_gallery`
                        WHERE `favorite_galleries`.`id_user` = '$userid'
                    ");
                $dellink = "favoritegalleries";
                if(!empty($rows)) 
                    foreach ($rows as $row) 
                    {
                ?>
                <div class="bit_body col-md-12 ">
                    <div class="borderRight col-md-6">
                        <span class="lot_photo"><img src="/img/gallery_default_image.png"/></span>
                        <p class="pheading">Name: {{$row->name}}</p>
                        <i class="pheading">Country: {{$row->country}} </i>
                        <p class="pheading">Address: {{$row->city . " " . $row->address}}$</p>
                    </div>
                    <div class="borderLeft col-md-6">
                        <p class="pheading">Contact: {{$row->contact_person}}</p>
                        <p class="pheading">Website: {{$row->website}}</p>
                    </div>
                    <a href="/usercabinet/{{$dellink}}/delete/{{$row->id}}" class="deleteBit">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>

                <?php
                    }
                    if(count($rows) == 0)
                    {
                        echo "No results found";
                    }
                break;
                }

            ?>

        </div>
            <br>


    </div>

    @endsection