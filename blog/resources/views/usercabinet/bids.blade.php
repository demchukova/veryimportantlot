@extends('layouts.app') @extends('layouts.nav') @extends('layouts.footer') @section('content') @include('layouts.usercabinetnav')
<?php
    $userdata = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
    $userid = $userdata->id;
    if (strlen($active) > 0)
    {
        $rows = DB::select("
            select rates.id, lot.id as 'lotid', all_users.first_name, all_users.last_name, lot.name as 'lot_name', lot.minimum_bid, catalog.name as 'from_catalog', catalog.exibition_end, rates.value as 'my_bit' from rates
            join all_users on all_users.id = rates.id_user
            join lot on lot.id = rates.id_lot
            join catalog on lot.id_catalog = catalog.id
            where all_users.id = '$userid' and exibition_end >= (select now());
        ");
        
        $pastrows = DB::select("
            select rates.id, lot.id as 'lotid', all_users.first_name, all_users.last_name, lot.name as 'lot_name', lot.minimum_bid, catalog.name as 'from_catalog', catalog.exibition_end, rates.value as 'my_bit' from rates
            join all_users on all_users.id = rates.id_user
            join lot on lot.id = rates.id_lot
            join catalog on lot.id_catalog = catalog.id
            where all_users.id = '$userid' and exibition_end < (select now());
        ");
        $activeLots = count($rows);
        $pastrows = count($pastrows);
    }
    else
    {
        $rows = DB::select("
            select rates.id, lot.id as 'lotid', all_users.first_name, all_users.last_name, lot.name as 'lot_name', lot.minimum_bid, catalog.name as 'from_catalog', catalog.exibition_end, rates.value as 'my_bit' from rates
            join all_users on all_users.id = rates.id_user
            join lot on lot.id = rates.id_lot
            join catalog on lot.id_catalog = catalog.id
            where all_users.id = '$userid' and exibition_end < (select now());
        ");
        $pastrows = DB::select("
            select rates.id, lot.id as 'lotid', all_users.first_name, all_users.last_name, lot.name as 'lot_name', lot.minimum_bid, catalog.name as 'from_catalog', catalog.exibition_end, rates.value as 'my_bit' from rates
            join all_users on all_users.id = rates.id_user
            join lot on lot.id = rates.id_lot
            join catalog on lot.id_catalog = catalog.id
            where all_users.id = '$userid' and exibition_end >= (select now());
        ");
        $activeLots = count($pastrows);
        $pastrows = count($rows);
    }
?>
    <div class="container" style="min-height: 700px;">
        <br>
        <br>
        <div class="row">
            <ul class="nav navCenter">
                <li class="{{$active}} col-md-6">
                    <a href="/usercabinet/bids" class="">Active<span class="drop_count">{{$activeLots}}</span></a>
                </li>
                <li class="{{$past}} col-md-6">
                    <a href="/usercabinet/pastbids">Past bids<span class="drop_count">{{$pastrows}}</span></a>
                </li>
            </ul>
            <br>
            <?php
                if(!empty($rows)) 
                    foreach ($rows as $row) 
                    {
                    $img = DB::select("
                        SELECT `link_to_image` FROM `lot_image` WHERE `id_lot` = '$row->lotid' LIMIT 1
                    ");
                    $img = $img[0];
            ?>
                <div class="bit_body col-md-12 ">
                    <div class="borderRight col-md-6">
                        <span class="lot_photo"><img src="/img/{{$img->link_to_image}}"/></span>
                        <p class="pheading">Lot: {{$row->lot_name}}</p>
                        <i class="pheading">Catalog: {{ $row->from_catalog }}</i>
                        <p class="pheading">minimal bid: {{ $row->minimum_bid }}$</p>
                    </div>
                    <div class="borderLeft col-md-6">
                        <p class="pheading">curent bid: {{$row->my_bit}}$</p>
                    </div>
                    <a href="/usercabinet/bids/delete/{{$row->id}}" class="deleteBit">
                        <i class="fa fa-trash"></i>
                    </a>
                </div>
                <?php
                }
                if(count($rows) == 0)
                {
                    echo "No results found";
                }
            ?>
        </div>
        <br>
    </div>

    <!--
    rise bit button
        <div class="bitButton">
            <input type="number" name="bitrize" value="10" min="10" class="bitInput" />
            <button class="bitRize">Rise</button>
        </div>

    -->

    @endsection