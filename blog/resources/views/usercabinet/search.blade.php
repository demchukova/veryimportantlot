
@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
@include('layouts.usercabinetnav')
<br><br>
<div class="container" style="min-height: 700px;">
    
    <div class="top_tab_auction">
            <div class="row">
                <ul class="nav nav-tabs mt15">
                    <li class="{{$page[0]}} col-md-6 col-sm-12 col-xs-12">
                        <a href="/usercabinet/search">All auction lots</a>
                    </li>
                    <li class="{{$page[1]}} col-md-6 col-sm-12 col-xs-12">
                        <a href="/usercabinet/searchGalleryItems">Galleries Items</a>
                    </li>
                </ul>
            </div>
    </div>
        <br>
        <br>
   


        <script>
            function activateBTN(i){
                if (i == 1)
                {
                    document.getElementsByClassName('th_list')[0].setAttribute('class', 'th_list activeBTN');
                    document.getElementsByClassName('th_large')[0].setAttribute('class', 'th_large');
                    document.getElementsByClassName('th_td')[0].setAttribute('class', 'th_td');
                }
                else if (i == 2)
                {
                    document.getElementsByClassName('th_list')[0].setAttribute('class', 'th_list');
                    document.getElementsByClassName('th_large')[0].setAttribute('class', 'th_large activeBTN');
                    document.getElementsByClassName('th_td')[0].setAttribute('class', 'th_td');   
                }
                else
                {
                    document.getElementsByClassName('th_list')[0].setAttribute('class', 'th_list');
                    document.getElementsByClassName('th_large')[0].setAttribute('class', 'th_large');
                    document.getElementsByClassName('th_td')[0].setAttribute('class', 'th_td activeBTN');  
                }
            }
            function changeToList(){
                activateBTN(1);
                
            }
            
            function changeToLarge(){
                activateBTN(2);
                
            }
            
            function changeToTd(){
                activateBTN(3);
                
            }
        </script>
    <br>    
 
    <?php
        $userid = DB::table('all_users')->select('id')->where('id_user', '=', Auth::user()->id)->first();
        $userid = $userid->id;
        if (strlen($page[0]) > 0)
        {
            $rows = DB::select("
                SELECT lot.`id`,lot.name, lot.`description`, lot.`minimum_bid` FROM `lot`
                WHERE 1 ORDER BY lot.`id` DESC;
            ");
            
            foreach ($rows as $row)
            {
                $img = DB::table('lot_image')->select('link_to_image')->where('id_lot', '=', $row->id)->first();
    ?>
    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-item-id="194" data-item-object-type="2">
                <div class="bl_auction_item">
                    <a data-pjax="0" href="/lot/{{$row->id}}" class="bl_img">
                        <img src="/img/{{$img->link_to_image}}" alt="">
                    </a>
                    <i class="fa fa-gavel" aria-hidden="true"></i>
                    <div class="auction_item_content">
                        <a data-pjax="0" href="/lot/194">
                            <span class="item_short_text">{{$row->name}}</span>
                        </a>
                        <span class="item_lot">Lot {{$row->id}}</span>
                        <table class="f18 item_table">
                            <tbody>
                                <tr>
                                    <td nowrap="">
                                        <span class="min_price_text">Minimum bid: </span>
                                    </td>
                                    <td>
                                        <span class="min_price_value">€{{$row->minimum_bid}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="item_time"></span>
                        <div style="clear: both"></div>
                        <div class="item_short_text2">{{$row->description}}</div>
                        <a href="/lot/{{$row->id}}" class="item_view_detail">View details</a>
                        <div class="auction_item_fav_btn">
                       <?php
                        
                                $favorite = "";
                                if (Auth::check() )
                                {
                                      $userdata = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
                                      $userid = $userdata->id;
                                    $f = DB::select("
                                        SELECT COUNT(*) as 'nr' FROM `favorite_lots` WHERE `id_lot` = '$row->id' AND `id_user` = '$userid';
                                    "); 
                                    $f = $f[0]->nr;
                                    if ($f > 0)
                                        $favorite = "active";
                                }    
                        ?>
                        <a href="/addtofavoritelots/{{$row->id}}" lot-favorite-id="194" class="auction_item_fav {{$favorite}} set-favorite">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </a>
                            <a href="" lot-calendar-id="194" class="auction_item_calendar" onclick="">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
    <?php
            }
            if (count($rows) == 0)
            {
                echo "No results found!";
            }
        }
        else if (strlen($page[1]) > 0)
        {
            $rows = DB::select("
                SELECT `gallery_item`.id, `gallery_item_image`.`link_to_image`, `gallery_item`.name, `gallery_item`.`price`, `gallery_item`.`description` FROM `gallery_item`
                JOIN `gallery_item_image` ON `gallery_item_image`.`id_gallery_item` = `gallery_item`.id
                WHERE 1 ORDER BY `gallery_item`.id DESC
            ");
            foreach ($rows as $row)
            {
            ?>
    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-item-id="1564" data-item-object-type="4">
        <div class="bl_auction_item">
            <a data-pjax="0" href="/lot-gallery/{{$row->id}}" class="bl_img">
            <img src="/img/{{$row->link_to_image}}" alt="">
        </a>
        <i class="fa fa-picture-o" aria-hidden="true"></i>
        <div class="auction_item_content">
            <a data-pjax="0" href="/lot-gallery/{{$row->id}}">
                <span class="item_short_text">{{$row->name}}</span>
            </a>
            <span class="item_lot">ID {{$row->id}}</span>
            <table class="f18 item_table">
                <tbody>
                <tr>
                    <td>
                        <span class="min_price_text">Price: </span>
                    </td>
                    <td>
                        <span class="min_price_value">${{$row->price}}</span>
                    </td>
                </tr>
                </tbody>
            </table>

            <div style="clear: both"></div>
            <div class="item_short_text2">{{$row->description}}</div>

            <a href="/lot-gallery/{{$row->id}}" class="item_view_detail">View details</a>
            <div class="auction_item_fav_btn">
                <a href="javascript:void(0);" lot-favorite-id="194" class="auction_item_fav  set-favorite">
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </a> 
            </div>
        </div>
    </div>
</div>
        <?php
            }
            if (count($rows) == 0)
            {
                echo "No results found!";
            }
        }
        ?>
</div>
    
@endsection