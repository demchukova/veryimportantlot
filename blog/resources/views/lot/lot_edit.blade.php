
@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
@include('layouts.auctioncabinetnav')
<?php 
    if (isset($err))
    {
        echo
            "
                <script>
                    alertBox('". $err ."');
                </script>
            ";
    }
?>
<div class="container">
    <form method="put" action="/auctioncabinet/lot/update/{{$lot->id}}" enctype="multipart/form-data">
        
        <table>
            <tr>
                {{ csrf_field() }}
                <td> Name: 
        <input type="text" name ="name" value="{{$lot->name}}"/></td>
            </tr>
        </table>
        <br>
        <br>
        description:
        <input type="text" name="description" value="{{$lot->description}}"></textarea>
        <br>
        <br>
        minimum bid: 
        <input type="number" name="minimum_bid" value="{{$lot->minimum_bid}}"/>
        <br>
        <br>
        <input type="file" name="link_to_image" />
        <br>
        <input type="submit" value="add lot">
    </form>
</div>

@endsection