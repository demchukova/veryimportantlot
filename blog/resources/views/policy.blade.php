@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container" style="min-height: 700px;">
    <div class="main" style="margin: 26px auto">
        <h1>Privacy policy</h1>

        <p>&nbsp;</p>

<p>&nbsp;</p>

<p>We thank you for visiting our website and your interest in our organization and our services. The protection of your personal data when using our website is important to us. In this regard, please take into consideration the following information:</p>

<p>&nbsp;</p>

<p><strong>1. Principles of working with personal data</strong></p>

<p>&nbsp;</p>

<p>You can visit our website without providing us with your personal data. Personal data will be required only for concluding a contract, for opening a personal account or for communication purposes. The provided personal data may and will be used without your unambiguous consent only for the purposes of execution of the terms of the contract and for processing of your requests. After the complete execution of the terms of the contract, your data will be stored in accordance with the tax and legal terms of storage, but will be blocked for any other purposes and subsequently removed at term expiration unless you confirm your consent for further use of your personal data.</p>

<p>&nbsp;</p>

<p>In addition, we obtain your personal data when you subscribe to our newsletters. This data will be used for promotional offers distributed in the form of newsletters, providing we receive your unambiguous consent for subscription. You can unsubscribe from the newsletters at any time by clicking on the designated link in the body of email or by sending us a request. After you unsubscribe, your email address will be immediately removed from the newsletter distribution database.</p>

<p>&nbsp;</p>

<p><strong>2. Transfer of personal data</strong></p>

<p>&nbsp;</p>

<p>If you pay directly via PayPal, by a credit card or from your debit account with PayPal, or in case of bidding through the &ldquo;Checkout Without a PayPal Account&rdquo; option, we report the date of your payment, as a part of the payment procedures to the PayPal company (Europe) at <em>S.&agrave; r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg, Luxembourg </em>(hereinafter &ldquo;PayPal&rdquo;). PayPal reserves the right to check the payer&rsquo;s creditability upon receipt of payments. The creditability analysis is used by PayPal when assessing the possibility of providing the appropriate payment method. Creditability information may contain probability values ​​ (so-called scoring). To a certain extent, a scoring system which is based on generally accepted scientific system of mathematical statistics reflects the buyer&rsquo;s solvency data. The evaluating system also tracks the addresses of the incoming data. For more information about the protection of personal data, including that collected and used by reference agencies, please refer to the privacy policy of PayPal which may be found online at <a href="https://www.paypal.com/de/webapps/mpp/ua/privacy-full">https://www.paypal.com/de/webapps/mpp/ua/privacy-full</a>.</p>

<p>&nbsp;</p>

<p><strong>3. Cookies </strong></p>

<p>&nbsp;</p>

<p>In order to make your visit to our website more pleasant and in order to enable certain online functions, many of our web pages use &ldquo;cookies&rdquo;. These imply small text files which are stored on your device. Some of the cookies (<em>session cookies</em>) are automatically removed when you close your browser. Other cookies remain on your device and allow us or our partner companies to recognize your device at your next visit (<em>persistent cookies</em>). You can set your browser so that you receive notification about installing cookies and make individual decisions whether to install them, or allow cookies only in certain situations or completely block the cookies. If you block cookies, some features of our website may be limited.</p>

<p>&nbsp;</p>

<p><strong>4. Google AdSense</strong></p>

<p>&nbsp;</p>

<p>This site uses Google AdSense, Internet alert service by Google Inc. (&quot;Google&quot;). Google AdSense uses so-called DoubleClick cookies (DART). These are text files stored on your computer which enables tracking your behaviour on our website. In addition, for the tracking purposes Google AdSense also uses &quot;Web-Beacons&quot; (small invisible graphics) which make it possible to take into account, collect and use the data on usual activities, such as visitors&rsquo; navigation around the site. The information about your web activity (including your IP-address), obtained by cookies and / or Web-Beacons, is usually sent and stored at the US server of Google. The Google AdSense reports are used by Google in order to determine your normal browsing behaviour. The IP-address transmitted by Google plug-ins installed into your browser, is used separately from other Google data. Information obtained from Google may be transmitted to a third party under certain conditions. You can block the cookies and the transmission of notifications by applying corresponding settings in your browser, or by downloading and installing the available browser plug-in using the following link: <a href="http://www.google.com/settings/ads/plugin?hl=de">http://www.google.com/settings/ads/plugin?hl=de</a>. Please note that certain features of this site may be unavailable or restricted, if you block or disable cookies.</p>

<p>&nbsp;</p>

<p><strong>5. Your rights and the establishment of contact</strong></p>

<p>&nbsp;</p>

<p>You have the right for a free reference to your stored data and, if necessary, the right to edit, block or delete the data. If you have questions about gathering, processing or use of your personal data, about access to the stored data, its editing, access restriction or about deleting the data, or in case of revoking of previously granted consent upon the data use, please contact us directly. Our contact address can be found in the designated section.</p>

<p>&nbsp;</p>
    </div>
</div>
<br>
<br>

@endsection