@section('footerBar')
<footer>
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer_top_title">@lang('msg.contact')</div>
                    <div class="footer_top_text">
                        <i class="fa fa-phone" aria-hidden="true"></i> +49 (0) 4298 699 64 54
                    </div>
                    <div class="footer_top_text"> <a href="mailto:support@veryimportantlot.com" class="a_footer_mail"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@veryimportantlot.com</a> </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_top_title">@lang('msg.partners')</div>
                    <a class="footer_top_text" href="/registerAuctionHouse">
                        @lang('msg.reg_auction')</a>
                    <a class="footer_top_text" href="/registerGallery">
                        @lang('msg.reg_gallery')</a>
                </div>
                <div class="col-md-6">
                    <div class="footer_top_title" style="height: 39px">@lang('msg.information')<ul class="social">
                            <li> <a href="http://facebook.com"> <i class="fa fa-facebook-official" aria-hidden="true"></i> </a> </li>
                            <li> <a href="https://plus.google.com"> <i class="fa fa-google" aria-hidden="true"></i> </a> </li>
                            <li> <a href="http://twitter.com"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                            <li> <a href=""> <i class="fa fa-vk" aria-hidden="true"></i> </a> </li>
                            <li> <a href="https://www.instagram.com/"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                        </ul>
                    </div>
                    <div class="row">


                        
            <div class="col-md-6">
                            <a href="/wiki/8" class="footer_top_text">
                    @lang('msg.terms')               </a>
                    </div>
            <div class="col-md-6">
                            <a href="/wiki/9" class="footer_top_text">
                    @lang('msg.imprint')               </a>
                    </div>
            <div class="col-md-6">
                            <a href="/wiki/10" class="footer_top_text">
                    @lang('msg.privacy')            </a>
                    </div>
            <div class="col-md-6">
                            <a href="/wiki/11" class="footer_top_text">
                    @lang('msg.glossary')               </a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <p>© Very Important Lot. @lang('msg.right').</p>
                </div>
                <div class="col-md-3">
                    <img src="/img/i_visa.png"  alt=""/>
                    <img src="/img/i_master.png"  alt=""/>
                    <img src="/img/i_amex.png" alt=""/>
                </div>
            </div>

        </div>
    </div>
</footer>
@endsection