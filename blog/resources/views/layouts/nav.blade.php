@section('navbar')
    <script>
        var nr = 0;
        function alertBox(msg)
        {
            var box = document.createElement('div');
            var close = document.createElement('span');
            var text = document.createElement('span');
            box.setAttribute('id', 'alertBox'+nr);
            text.innerHTML = msg;
            close.innerHTML = '×';
            text.setAttribute('class', 'alertText');
            close.setAttribute('class', 'alertClose');
            close.setAttribute('onclick', "document.getElementById('alertBox"+nr+"').style.display = 'none';");
            box.setAttribute('class', 'alertBox');
            box.appendChild(text);
            box.appendChild(close);
            document.getElementById('alertArea').appendChild(box);
            box.style.opacity = '1';
            setTimeout(function(){
                 setTimeout(function(){
                     box.style.display = 'none';
                 }, 1000);
                 box.style.opacity = '0';
            }, 10000);
            nr++;
        }
    </script>
<?php
    if (!isset($nav))
    {
        $nav = ["","","",""];
    }
?>
<div id="alertArea"></div>
<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="/">
                <span class="logo_header"></span>   
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse" style="text-transform: uppercase;font-weight:bold;">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li><a href="/" class="{{$nav[0]}}">@lang('msg.home')</a></li>
                <li><a href="/auction/catalog-list" class="{{$nav[1]}}">@lang('msg.auctions')</a></li>
                <li><a href="/auction/catalog-archive-list" class="{{$nav[2]}}">@lang('msg.results')</a></li>
                <li><a href="/gallery/gallery-list" class="{{$nav[3]}}">@lang('msg.galleries')</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" onclick="
                        document.getElementById('searchBlockBg').style.display = 'block';
                        document.getElementById('searchBlock').style.display = 'block';
                        document.getElementById('searchBlock').style.opacity = '0';
                        document.getElementById('searchBlockBg').style.opacity = '0';
                        setTimeout(function(){
                            document.getElementById('searchBlock').style.opacity = '1';
                            document.getElementById('searchBlockBg').style.opacity = '1';
                        }, 100);
                        " class="searchBtn"></a>
                </li>
                    <li class="dropdown">
                        <a class="dropbtn">@lang('msg.lang')</a>
                        <div class="dropdown-content">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                {{ $properties['native'] }}
                            </a>
                        @endforeach
                        </div>
                    </li>

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="/login" ><span class="reg-login-img"></span>@lang('msg.log_in')</a></li>
                    <li><a href="/register" ><span class="reg-register-img"></span>@lang('msg.register')</a></li>
                @else
                    <?php
                        $userid = Auth::user()->id;
                        $role = DB::select("SELECT role FROM users WHERE id = '$userid';");
                        $role = $role[0]->role;
                        $red = "/usercabinet";
                        switch($role)
                        {
                            case 1:
                                $red = "/usercabinet";
                                break;
                            case 2:
                                $red = "/gallerycabinet";
                                break;
                            case 3:
                                $red = "/auctioncabinet";
                                break;
                        }
                    ?>
                    <li>
                        <a href="{{$red}}" class="logged_in">
                        </a>
                    </li>
                    <li><a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();"><span class="reg-login-img"></span>@lang('msg.log_out')</a>
                </li>
                
                @endif
            </ul>
        </div>
    </div>
</nav>
<div class="searchInput" id="searchBlockBg" onclick="
                        document.getElementById('searchBlock').style.opacity = '0';
                        this.style.opacity = '0';
                        setTimeout(function(){
                            document.getElementById('searchBlock').style.display = 'none';
                            document.getElementById('searchBlockBg').style.display = 'none';
                        }, 1000);
                        "></div>
<div class="container z_ind" id="searchBlock">
    <input type="text" class="searchInputText col-sm-10" plceholder="Search"/>
    <button class="col-sm-2 dark " style="  border: none;
                                            padding: 11px;
                                            border: 1px solid white;">Search</button>
    <br>
    <br>
    <br>
    <p style="color:white;">No search results...</p>
</div>

<div class="logBG" id="logBG" onclick="closeBoth()"></div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
@endsection