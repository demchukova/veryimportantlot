<div class="" style="">
    <?php
        $userdata = DB::table('users')->where('id', '=', Auth::id())->first();
        $auctiondata = DB::table('auction_house')->where('id_user', '=', Auth::id())->first();
        $catalogs = DB::table('catalog')->where('id_auction_house', '=',$auctiondata->id)->get();
    ?>
    <div class="lk_nav_menu" style="border-bottom:1px solid #bababa;">
        <div class="container">
            <ul>
                <li>
                    <a href="/auctioncabinet/search" class="<?php echo $searchClass; ?>">Search</a>
                </li>
                <li>
                    <a href="/auctioncabinet/catalog" class="<?php echo $catalogClass; ?>">
                    Catalog<span class="drop_count">
                        <?php
                            echo DB::table('catalog')->where('id_auction_house', $auctiondata->id)->count();
                        ?>
                    </span>
                </a>
                </li>
                <li>
                    <a href="/auctioncabinet/lot" class="<?php echo $lotClass; ?>">
                    Lot
                </a>
                </li>
                <li>
                    <a href="/auctioncabinet/bids" class="<?php echo $bidsClass; ?>">Bids
                </a>
                </li>
                <li class="lk_menu_user_setings">
                    <a href="/auctioncabinet" class="<?php echo $setingsClass; ?>"><i class="fa fa-cogs" aria-hidden="true"></i> 
                {{ $userdata->email }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>