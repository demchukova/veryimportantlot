<div style="min-height: 600px">
    <?php
        $userid = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
        $userid = $userid->id;
    ?>
    <div class="lk_nav_menu" style="border-bottom:1px solid #bababa;">
        <div class="container">
            <ul class="">
                <li>
                    <a href="/usercabinet/search" class="<?php echo $searchClass; ?>">Search</a>
                </li>
                <li>
                    <a href="/usercabinet/calendar" class="<?php echo $calendarClass; ?>">
                    Calender<span class="drop_count">0</span>
                    </a>
                </li>
                <li>
                    <a href="/usercabinet/favorites" class="<?php echo $favoritesClass; ?>">
                    Favorites<span class="drop_count">
                        <?php
                            $nr = DB::select("
                                 select count(*) as 'val' from (
                                 select * from favorite_catalogs 
                                 where id_user = '$userid'
                                 union 
                                 select * from favorite_galleries
                                 where id_user = '$userid'
                                 union 
                                 select * from favorite_gallery_item
                                 where id_user = '$userid'
                                 union 
                                 select * from favorite_lots
                                 where id_user = '$userid')t1;
                            ");
                        echo $nr[0]->val;
                        ?>
                        </span>
                </a>
                </li>
                <li>
                    <a href="/usercabinet/bids" class="<?php echo $bidsClass; ?>">Bids<span class="drop_count">
                        <?php
                            $nr = DB::select("
                                select count(value) as 'val' from rates where id_user = '$userid';
                            ");
                        echo $nr[0]->val;
                        ?>
                        </span>
                </a>
                </li>
                <li class="lk_menu_user_setings">
                    <a href="/usercabinet" class="<?php echo $setingsClass; ?>"><i class="fa fa-cogs" aria-hidden="true"></i> 
                <?php 
                        $userdata = DB::table('users')->where('id', '=', Auth::id())->first();
                        echo $userdata->email; 
                ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>