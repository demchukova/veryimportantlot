@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section ('content')

     <div class="container">

    <ul class="breadcrumb"><li><a href="/">Home</a></li>
<li><a href="/gallery/gallery-list">All galleries</a></li>
<li><a href="/gallery/{{$gallery->id}}">{{$gallery->name}}</a></li>
<li class="active">{{$item->name}}</li>
</ul>
</div>

<div style="min-height: 600px">
<div class="container bl_prod_page bl_galleries_product_page" data-item-id="427" data-item-object-type="4" >
    <div class="row">
        <div class="col-md-6">
<div id="bl_prod_carousel" class="carousel slide" data-ride="carousel">
    <div class="prod_carousel_top">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php $first = 1; ?>
            @foreach ($item_image as $image)
                @if ($first == 1)
                    <div class="item active">
                        <a class="bl_img fancybox" href="#" data-fancybox-group="gallery" title="">
                            <img src="/img/{{$image->link_to_image}}"  alt=""/>
                        </a>
                    </div>
                    <?php $first = 0; ?>
                @else
                    <div class="item">
                        <a class="bl_img fancybox" href="#" data-fancybox-group="gallery" title="">
                            <img src="/img/{{$image->link_to_image}}"  alt=""/>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#bl_prod_carousel" role="button" data-slide="prev">
            <span><i class="fa fa-angle-left" aria-hidden="true"></i></span>
        </a>
        <a class="right carousel-control" href="#bl_prod_carousel" role="button" data-slide="next">
            <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        </a>
    </div>
    <!-- Indicators -->
    <div class="row">
        <ol class="carousel-indicators">
                @foreach ($item_image as $image)
                    @if ($image->logo == 0)
                        @if ($first == 0)
                        <li data-target="#bl_prod_carousel" data-slide-to="{{$first++}}" class="active">
                            <div class="bl_img"><img src="/img/{{$image->link_to_image}}"  alt=""/></div>
                        </li>
                        @else
                        <li data-target="#bl_prod_carousel" data-slide-to="{{$first++}}" class="">
                            <div class="bl_img"><img src="/img/{{$image->link_to_image}}"  alt=""/></div>
                        </li>
                        @endif
                    @endif
                @endforeach
        </ol>
    </div>
</div>
        </div>
        <div class="col-md-6  bl_prod_right">
            <div class="row">
                <div class="col-md-12 c_gray">
                    <span class="d_table f16" style="font-size:16px;">Estimate value:</span> <br>
                    <span class="d_table f48" style="font-size:48px;">₽{{$item->price}}</span>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 gallerie_product_contact mt30">
                    <div class="product_contact_title">Contact</div>
                    <a href="mailto:knigocheis@mail.ru" class="product_contact"><span><i class="fa fa-envelope" aria-hidden="true"></i></span>
                        {{$user->email}}                   </a>
                    <span class="product_contact"><span><i class="fa fa-phone" aria-hidden="true"></i></span>{{$gallery->phone_number}}</span>
                </div>

            </div>
        </div>
    </div>
@if(Auth::check())
    @if(Auth::user()->role == 2)
        @if(Auth::user()->id == $user->id)
    <div class="row">
        <div class="col-md-12" style=" margin: 5px;">
            <form action="/gallery/image/add/{{$id_item}}" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
            <div class="form-group">
              <input type="file" id="file" name="file" required> 
            </div>
            
            <button type="submit" class="btn btn-success">Add Image</button>
            
          </form>
        </div>
    </div>
        @endif
    @endif
@endif
    <div class="row">
        <div class="col-md-12 mt70 ">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#opisanie" aria-controls="home" role="tab" data-toggle="tab">Description</a></li>
                <li role="presentation"><a href="#contact" aria-controls="messages" role="tab" data-toggle="tab">Contact</a></li>
                <!-- li role="presentation"><a href="#oplata" aria-controls="oplata" role="tab" data-toggle="tab">Оплата</a></li -->
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="opisanie">
                    <div class="row">
                        <div class="col-md-10">
							<span class="f18">
								{{$item->description}}
							</span>
                        </div>

                        <!-- div class="col-md-2">
                            <img src="/img/g_translate.jpg" width="152" height="35" alt=""/>
                        </div -->
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="contact">
                    <div class="bl_header_tab_info">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="bl_logo_img">
                                    <img src="/img/{{$logo[0]->link_to_image}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <span class="header_tab_info_title">Address</span>
                                <span>{{$gallery->name}}</span>
                                <span>{{$gallery->address}}</span>
                                <span>{{$gallery->city}}</span>
                                <span>{{$gallery->country}}</span>
                            </div>
                            <div class="col-md-3">
                                <span class="header_tab_info_title">Contact</span>
                                <a href="javascript:;" class="f18 mb25 a_black d_inblock"><i class="fa fa-envelope mr10" aria-hidden="true"></i>{{$user->email}} </a>
                                                                    <span class="f18"><i class="fa fa-phone mr14" aria-hidden="true"></i>{{$gallery->phone_number}} </span>
                                                                                                                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                </div>
                <!--  div role="tabpanel" class="tab-pane" id="oplata">
                    текст оплата <br> Fineart-Auction ist ein Portal für Kunst- undauch andere Auktionen und bietet Webseitenfunktionalitäten und andere Servicesan, wenn Sie die Fineart-Auction- Webseite oder Services von Fineart-Auctionnutzen, Fineart-AuctionMobile-Applikationen verwenden oder in einem der vorgehenden Zusammenhänge von Fineart-Auction bereitgestellte Software nutzen(zusammen die \" Fineart-AuctionServices\"). Fineart-Auctionstellt die Fineart-AuctionServices zu den auf dieser Seite angegebenen Allgemeinen Geschäftsbedingungenbereit.
                </div -->
            </div>
        </div>
    </div>
</div>

</div>

@endsection