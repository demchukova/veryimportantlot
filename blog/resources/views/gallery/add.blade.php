
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add element</h4>
        </div>
        <div class="modal-body">
          <form action="/gallery/add" method="POST" enctype="multipart/form-data">
               {{ csrf_field() }}
            
            <div class="form-group">
              <input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
            </div>
            
            <div class="form-group">
              <input type="text" class="form-control" id="price" name="price" placeholder="Price" required>
            </div>
            
            <div class="form-group">
              <textarea class="form-control" rows="5" id="description" name="description" placeholder="Description" required></textarea>
            </div>
            
            <div class="form-group">
              <input type="file" id="file" name="file" required> <br>
            </div>
            
            <button type="submit" class="btn btn-success">Add Item</button>
            
          </form>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



<!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Gallery Image</h4>
        </div>
        <div class="modal-body">
            <div class="row" style="margin-bottom:15px; font-size: large; font-weight: bold;">
              <div class="col-md-6">
                Backgroud Image Change
              </div>
              <div class="col-md-6">
                Logo Image Change
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <form action="/gallery/changes_bg" method="POST" enctype="multipart/form-data">
                     {{ csrf_field() }}
                  
                  <div class="form-group">
                    <input type="file" id="file" name="file" required> <br>
                  </div>
                  
                  <button type="submit" class="btn btn-success">Change image</button>
                  
                </form>
              </div>
              <div class="col-md-6">
                <form action="/gallery/changes_logo" method="POST" enctype="multipart/form-data">
                     {{ csrf_field() }}
                  
                  <div class="form-group">
                    <input type="file" id="file" name="file" required> <br>
                  </div>
                  
                  <button type="submit" class="btn btn-success">Change image</button>
                  
                </form>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
