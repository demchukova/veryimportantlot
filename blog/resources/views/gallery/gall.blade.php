<?php
    $count_items = DB::table('gallery_item')->select('*')->where('id_gallery', '=', $gall->id)->count();
    $gallery_image = DB::table('gallery_image')->select('link_to_image')->where('id_gallery', '=', $gall->id)->latest()->get();
    $logo = DB::table('gallery_item_image')->select('link_to_image')->where('logo', '=', 1)->where('id_gallery_item', '=', $gall->id)->latest()->get();
    //dd($logo);
?>

<div class="tab_auction_12_item all_gallery_tab">
    <div class="bl_img">
        @if($gallery_image->count() == 0)
            <img src="https://mvlnnq.dm2301.livefilestore.com/y3mSwl_fG7ujZH5fFQ3txsKizXyglyTuvv8hPOagWpfTFPl93p6tPT6sfzt74-Zt8U4mt-YP2lh1SKWBSE54DetT8ZdMa__Iw4DhXkNAMxCIlMniOitsMJ0rpLwTatUGVpfRYqJ9WC28TfQ_ZFgvhw-zOsSlRnOltHGOMc20EbHxgw?width=960&height=252&cropmode=none" alt=""/>
        @else
            <img src="/img/{{$gallery_image->first()->link_to_image}}" alt=""/>
        @endif
    </div>
    <div class="item_12_content">
        <div class="item_12_content_left">
            <div class="bl_img">
                @if($logo->count() == 0)
                    <img src="http://getbg.net/upload/full/www.GetBg.net_3D-graphics__000817_1.jpg" alt=""/>
                @else
                    <img src="/img/{{$logo[0]->link_to_image}}" alt=""/>
                @endif
            </div>
        </div>
        <div class="item_12_content_right">
            <div class="item_12_auction_name">{{ $gall->name }}</div>
            <div class="item_12_auction_count">{{ $count_items }} items</div>
        </div>
        <a href="/gallery/{{$gall->id}}" class="item_12_content"></a>
        <div class="item_12_auction_date">
            <span class="span_date">Premium Pro</span>s
        </div>
        <div class="auction_12_calendar_fav">
            <a href="#">
                <span>Favorites</span>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>