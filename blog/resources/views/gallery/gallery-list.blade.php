@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container">
    <ul class="breadcrumb"><li><a href="/">Home</a></li>
        <li class="active">All galleries</li>
    </ul>
</div>

<div class="container" style="min-height: 700px;">
    <div class="top_tab_auction">
        <div class="container" style="min-height: 750px">
            <div class="row">
                <ul class="nav nav-tabs mt20">
                    <li class="active col-md-6 col-sm-12 col-xs-12">
                        <a href="/gallery/gallery-list">All galleries</a>
                    </li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <a href="/lot-gallery/list">All galleries items</a>
                    </li>
                </ul>
                <div id="product-list" class="" style="margin-top:50px;">
                    <div class='d_table w100p f0'>
 <!--php---------------------------------------------->             
                    @foreach ($gallery as $gall)
                        @include ('gallery.gall')
                    @endforeach
 <!--php----------------------------------------------> 
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection