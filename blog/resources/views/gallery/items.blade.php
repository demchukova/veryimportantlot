@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section ('content')

<div class="container">

    <ul class="breadcrumb"><li><a href="/">Home</a></li>
<li><a href="/gallery/gallery-list">All galleries</a></li>
<li class="active">items</li>
</ul>
</div>

<div style="min-height: 600px">
    <div class="bl_cat_auction_header bl_gallery_cat">
    

<div class="bl_img">
        @if($gallery_image->count() == 0)
            <img src="https://mvlnnq.dm2301.livefilestore.com/y3mSwl_fG7ujZH5fFQ3txsKizXyglyTuvv8hPOagWpfTFPl93p6tPT6sfzt74-Zt8U4mt-YP2lh1SKWBSE54DetT8ZdMa__Iw4DhXkNAMxCIlMniOitsMJ0rpLwTatUGVpfRYqJ9WC28TfQ_ZFgvhw-zOsSlRnOltHGOMc20EbHxgw?width=960&height=252&cropmode=none" alt=""/>
        @else
            <img src="/img/{{$gallery_image[0]->link_to_image}}" alt=""/>
        @endif
</div>
<div class="container">
    <div class="row ">
        <div class="d_table w100p plr9"  data-item-id="28" data-item-object-type="3" >

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6  bl_prod_img">

                
                <a href="{{$gallery->website}}" target="_blank" class="bl_img prod_img">
                    @if($logo->count() == 0)
                        <img src="http://getbg.net/upload/full/www.GetBg.net_3D-graphics__000817_1.jpg" alt=""/>
                    @else
                        <img src="/img/{{$logo[0]->link_to_image}}" alt=""/>
                    @endif
                    <span>
                        <i class="fa fa-external-link" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                <div class="auction_header_text">{{ $gallery->name }}</div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 auction_header_right">

            </div>
        </div>
    </div>
    <!-------------------------------------------------------------------------------------------->
    </div>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <div class=" bl_auction_list col_12">
                <div id="lot-list" class="">
                    
                    
                    @foreach ($gallery_items as $item)
                    <?php
                        $image = DB::table('gallery_item_image')->select('link_to_image')->where('id_gallery_item', '=', $item->id )->first();
                    ?>
                    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <div class="bl_auction_item">
                            <a href="/lot-gallery/{{$item->id}}" class="bl_img">
                                <img src="/img/{{$image->link_to_image}}" alt=""/>
                            </a>
                            <div class="auction_item_content">
                                <a href="/lot-gallery/{{$item->id}}">
                                    <span class="item_short_text">{{ $item->name }}</span>
                                </a>
                                <span class="item_lot">ID {{ $item->id }}</span>
                                <table class="f18 item_table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="min_price_text">Price: </span>
                                        </td>
                                        <td>
                                            <span class="min_price_value">₽ {{ $item->price }}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div style="clear: both"></div>
                                <div class="item_short_text2">
                                    {{ $item->description }}
                                </div>
                                <a href="/lot-gallery/{{$item->id}}" class="item_view_detail">View details</a>
                                <div class="auction_item_fav_btn">
                                    <a href="javascript:void(0);" lot-favorite-id="427" class="auction_item_fav  set-favorite " data-msg="Эта функция доступна только зарегистрированным пользователям" >
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection