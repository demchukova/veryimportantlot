@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section ('content')

<div class="container">

    <ul class="breadcrumb"><li><a href="/">Home</a></li>
<li><a href="/lot-gallery/list">All galleries items</a></li>
</ul>
</div>

<div>
    <div class="bl_cat_auction_header bl_gallery_cat"></div>
</div>
    <div class="top_tab_auction">
        <div class="container">
            <div class="row">
                <ul class="nav nav-tabs mt20">
                    <li class="active col-md-6 col-sm-12 col-xs-12">
                        <a href="/gallery/gallery-list">All galleries</a>
                    </li>
                    <li class="col-md-6 col-sm-12 col-xs-12">
                        <a href="/lot-gallery/list">All galleries items</a>
                    </li>
                </ul>
          </div>
     </div>
</div>
    <div class="container" style="margin-top:40px;">
        <div class="row">
            <div class=" bl_auction_list col_12">
                <div id="lot-list" class="">
                    
                    
                    @foreach ($gallery_items as $item)
                    <?php
                        $image = DB::table('gallery_item_image')->select('link_to_image')->where('id_gallery_item', '=', $item->id )->first();
                    ?>
                    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <div class="bl_auction_item">
                            <a href="/lot-gallery/{{$item->id}}" class="bl_img">
                                <img src="/img/{{$image->link_to_image}}" alt=""/>
                            </a>
                            <div class="auction_item_content">
                                <a href="/lot-gallery/{{$item->id}}">
                                    <span class="item_short_text">{{ $item->name }}</span>
                                </a>
                                <span class="item_lot">ID {{ $item->id }}</span>
                                <table class="f18 item_table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="min_price_text">Price: </span>
                                        </td>
                                        <td>
                                            <span class="min_price_value">₽ {{ $item->price }}</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div style="clear: both"></div>
                                <div class="item_short_text2">
                                    {{ $item->description }}
                                </div>
                                <a href="/lot-gallery/{{$item->id}}" class="item_view_detail">View details</a>
                                <div class="auction_item_fav_btn">
                                    <a href="javascript:void(0);" lot-favorite-id="427" class="auction_item_fav  set-favorite " data-msg="Эта функция доступна только зарегистрированным пользователям" >
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection