
@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')


@section('content')
@include('layouts.auctioncabinetnav')
<?php 
    if (isset($err))
    {
        echo
            "
                <script>
                    alertBox('". $err ."');
                </script>
            ";
    }
?>

<div class="container" style="min-height: 700px;">
    <br>
    <div class="well well-lg">
        <form class="form-horizontal" method="post" action="/auctioncabinet/lot/add/{{$id}}" enctype="multipart/form-data">
            {{ csrf_field() }}
  <div class="form-group">
       <label class="control-label col-sm-2" for="text">Name:</label>
           <div class="col-sm-10">
              <input type="text" required class="form-control" name="name" placeholder="Enter name"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Description:</label>
           <div class="col-sm-10">
             <textarea required class="form-control" name="description" value="{{old('description')}}"></textarea>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Minimum bid:</label>
           <div class="col-sm-10">
              <input type="number" required name="minimum_bid" required class="form-control"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Image</label>
           <div class="col-sm-10">
              <input type="file" name="link_to_image[]" multiple required style="position: absolute; top:8px;"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local"></label>
                <div class="col-sm-10">
                   <button type="submit" class="btn btn-default">Add lot</button>
               </div>
  </div>
        </form>
</div>
    
    
    
</div>
@endsection