@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')




@section('content')
@include('layouts.auctioncabinetnav')

<div class="container" style="min-height: 700px;">
    <br>
    <div class="well well-lg">
        <form class="form-horizontal" action="/auctioncabinet/catalog/updatecatalog/{{$catalog->id}}" method="put" enctype="multipart/form-data">
            {{ csrf_field() }}
  <div class="form-group">
       <label class="control-label col-sm-2" for="text">Name:</label>
           <div class="col-sm-10">
              <input type="text" required class="form-control" name="name" value="{{$catalog->name}}"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Auction date:</label>
           <div class="col-sm-10">
              <input type="datetime-local" required class="form-control" name="datehour" value="{{$catalog->datehour}}"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Exibition start:</label>
           <div class="col-sm-10">
              <input type="date" required class="form-control" name="exibition_start" value="{{$catalog->exibition_start}}"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Exibition end:</label>
           <div class="col-sm-10">
              <input type="date" required class="form-control" name="exibition_end" value="{{$catalog->exibition_end}}"/>
           </div>
  </div>
    <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Background image:</label>
           <div class="col-sm-10">
              <input type="file" required name="bg_image" style="position: absolute; top:8px;"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Logo image:</label>
           <div class="col-sm-10">
               <input type="file" required name="image"  style="position: absolute; top:8px;"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Conditions:</label>
           <div class="col-sm-10">
                <textarea required class="form-control" name="conditions" value="{{$catalog->conditions}}" rows="10"></textarea>
           </div>
  </div>
  <div class="form-group">
      <div class="col-sm-2"></div>
           <div class="col-sm-10">
               <button type="submit" class="btn btn-default">Edit</button>
           </div>
  </div>
        </form>
</div>
    
    
    
>>>>>>> 247d07ce7fdb1c1a6b29000defc542fb0bc65a8b
</div>

@endsection