@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')




@section('content')
@include('layouts.auctioncabinetnav')
<br>
<div class="container" style="min-height: 700px;">
    <div class="well well-lg">
        <form class="form-horizontal" action="/auctioncabinet/catalog/add" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
  <div class="form-group">
       <label class="control-label col-sm-2" for="text">Name:</label>
           <div class="col-sm-10">
              <input type="text" required class="form-control" name="name" placeholder="Enter name"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Auction date:</label>
           <div class="col-sm-10">
              <input type="datetime-local" required class="form-control" name="datehour" placeholder="Enter auction date"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Exibition start:</label>
           <div class="col-sm-10">
              <input type="date" required class="form-control" name="exibition_start" placeholder="Enter exibition start"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Exibition end:</label>
           <div class="col-sm-10">
              <input type="date" required class="form-control" name="exibition_end" placeholder="Enter exibition end"/>
           </div>
  </div>
    <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Background image:</label>
           <div class="col-sm-10">
              <input type="file" required name="bg_image" style="position: absolute; top:50%;"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Logo image:</label>
           <div class="col-sm-10">
               <input type="file" required name="image"  style="position: absolute; top:50%;"/>
           </div>
  </div>
  <div class="form-group">
       <label class="control-label col-sm-2" for="datetime-local">Conditions:</label>
           <div class="col-sm-10">
                <textarea required class="form-control" name="conditions"></textarea>
           </div>
  </div>
  <div class="form-group">
      <div class="col-sm-2"></div>
           <div class="col-sm-10">
               <button type="submit" class="btn btn-default">Create</button>
           </div>
  </div>
        </form>
    </div>
</div>
@endsection