@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container">
    <ul class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="/auction/catalog-list">All current catalogs</a></li>
        <li class="active">Schmuck und Uhren</li>
    </ul>
</div>
<div class="container" id="header_img" style="min-height: auto">
        <?php
         function isUser(int $id)
        {
        
        $is_user =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_user[0]->role == 1)
        return 1;
        else return 0;
        }
            $favorite = "-o";
            if (Auth::check() && isUser(Auth::id()))
            {
                $userdata = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
                $userid = $userdata->id;
                
                $f = DB::select("
                    SELECT COUNT(*) as 'nr' FROM `favorite_catalogs` WHERE `id_catalog` = '$catalog->id' AND `id_user` = '$userid' ;
                ");   
                if (isset($f))
                {
                    $f = $f[0]->nr;
                    if ($f > 0)
                        $favorite = "";
                }
            }
            $catalogs = DB::select("select * from catalog where id = '$catalog->id'");
            $id_ah = $catalogs[0]->id_auction_house;
            $auctiondata = DB::select("select * from auction_house where id = '$id_ah'");
            $catalogs = $catalogs[0];
            $auctiondata = $auctiondata[0];
        ?>
    <div class="bl_img" style="height: 300px; ">
        <img style="margin: 0 auto; width: 100%; height: auto;" src="/img/{{$catalogs->bg_image}}" alt="">
    </div>
</div>


<!----------------------------------------------------------------->

<div class="container">
        <div class="row ">
            <div class=" d_table w100p plr9" data-item-id="123" data-item-object-type="1">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6  bl_prod_img">
                    
                    <a href="" target="_blank" class="bl_img prod_img">
                        <img src="/img/{{$catalogs->image}}" alt="">
						<span>
							<!-- Переход<br>на сайт<br>каталога -->
							<i class="fa fa-external-link" aria-hidden="true"></i>
						</span>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                    <div class="auction_header_title">Catalog Name: {{ $catalogs->name }}</div>
                    <div class="auction_header_text">Auction House: {{$auctiondata->name}}</div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 auction_header_right">
                    <div>
                        <span class="bl_date">
                            <span style="font-size: 14pt;">Date of auction: </span>{{ $catalogs->datehour }}
                        </span>
                    </div>


                    <a href="/addtofavoritecatalog/{{$catalog->id}}" catalog-favorite-id="123" class="bl_i_fav show-login">
                        <span>Favorites</span>
                        <i class="fa fa-star{{$favorite}}" aria-hidden="true"></i>
                    </a>

                                        <a href="javascript:;" catalog-calendar-id="123" class=" auction_12_calendar bl_i_calendar" onclick="CUser.showLogin('Эта функция доступна только зарегистрированным пользователям')">
                        <span>Calender</span>
                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                        <i class="fa fa-calendar-o" aria-hidden="true"><span>5</span></i>
                    </a>

                    <a href="javascript:;" class="bl_i_share" data-toggle="modal" data-target="#share_modal">
                        <span>Share</span>
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </a>

                </div>
            </div>
        </div>
    </div>
    
<!-------------------------------------------------------------------->
<hr>
<!-------------------------------------------------------------------->
<?php
    $userid = Auth::id();
    $bo = DB::select("SELECT `id` FROM `auction_house` WHERE id_user = '$userid'");  
    $cat = DB::select("SELECT `id_auction_house` FROM `catalog` WHERE id = '$catalog->id'");  
    if(empty($bo) || empty($cat))
        $boo = 0;
    else if ($bo[0]->id == $cat[0]->id_auction_house)
        $boo = 1;
    else
        $boo = 0;
    if (Auth::check() && $boo)
    {
?>
    <div class="container">
    <a href="/auctioncabinet/lot/{{$catalog->id}}"><button type="button" class="btn btn-success" >+ Add Lot</button></a>
    </div>
<?php
    }
?>
<br>
<br>

<!-------------------------------------------------------------------->
<div class="container">
    <?php        
            $rows = DB::select("
                SELECT lot.`id`,lot.name, lot.`description`, lot.`minimum_bid`, catalog.`datehour` FROM `lot`
          		JOIN `catalog` ON `catalog`.`id` = `lot`.`id_catalog`
          		WHERE lot.id_catalog = '$catalog->id' ORDER BY lot.`id` DESC;
            ");
           
            foreach ($rows as $row)
            {
            $img = DB::select("
                SELECT `link_to_image` FROM `lot_image` WHERE `id_lot` = '$row->id' LIMIT 1
            ");
            $img = $img[0];
    ?>
    <div class="auction_item col-lg-12 col-md-12 col-sm-12 col-xs-12" data-item-id="194" data-item-object-type="2">
                <div class="bl_auction_item">
                    <a data-pjax="0" href="/lot/{{$row->id}}" class="bl_img">
                        <img src="/img/{{$img->link_to_image}}" alt="">
                    </a>
                    <i class="fa fa-gavel" aria-hidden="true"></i>
                    <div class="auction_item_content">
                        <a data-pjax="0" href="/lot/{{ $row->id }}">
                            <span class="item_short_text">{{$row->name}}</span>
                        </a>
                        <span class="item_lot">Lot {{$row->id}}</span>
                        <table class="f18 item_table">
                            <tbody>
                                <tr>
                                    <td nowrap="">
                                        <span class="min_price_text">Minimum bid: </span>
                                    </td>
                                    <td>
                                        <span class="min_price_value">€{{$row->minimum_bid}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <span class="item_time"></span>
                        <div style="clear: both"></div>
                        <div class="item_short_text2">{{$row->description}}</div>
                        <a href="/lot/{{$row->id}}" class="item_view_detail">View details</a>
                        <div class="auction_item_fav_btn">
                        <?php
                                $favorite = "";
                                if (Auth::check())
                                {
                                    $f = DB::select("
                                        SELECT COUNT(*) as 'nr' FROM `favorite_lots` WHERE `id_lot` = '$row->id' AND `id_user` = '$userid';
                                    "); 
                                    $f = $f[0]->nr;
                                    if ($f > 0)
                                        $favorite = "active";
                                }    
                        ?>
                        <a href="/addtofavoritelots/{{$row->id}}" lot-favorite-id="194" class="auction_item_fav {{$favorite}} set-favorite">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </a>
                            <a href="#" lot-calendar-id="194" class="auction_item_calendar" onclick="">
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </a>
                        @if (Auth::check() && $boo)
                            <a class="btn btn-danger" href="/auctioncabinet/catalog/delete/lot/{{$row->id}}"><i class="fa fa-trash-o fa-lg"></i> Delete</a>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
    <?php
            }
            if (count($rows) == 0)
            {
                echo "No results found!<br><br>";
            }
    ?>
</div>
@endsection