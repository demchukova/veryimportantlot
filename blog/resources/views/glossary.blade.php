@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container" style="min-height: 700px;">
    <div class="main" style="margin: 26px auto">
        <h1>Glossary</h1>

        <p><strong>Key concepts used on&nbsp;&laquo;VERY IMPORTANT LOT&raquo;&nbsp;</strong></p>

<p>&nbsp;</p>

<p><strong>Auction &ndash; </strong>the trading process in which the potential buyers (bidders) call their bids in order to win the auction. Auction is won by the bid with the highest price.</p>

<p>&nbsp;</p>

<p><strong>Auction Portal</strong> - an online resource where various auction houses can house their catalogues, and run online or live auctions.</p>

<p>&nbsp;</p>

<p><strong>Bidder</strong> &ndash; an individual or legal entity who submits a price offer for the object of bidding (lot).</p>

<p>&nbsp;</p>

<p><strong>Buyer</strong> &ndash; the bidder who won the auction and purchased the merchandise.</p>

<p>&nbsp;</p>

<p><strong>Bid</strong> <strong>&ndash;</strong> the price which is offered by the bidder for the lot in order to win the auction.</p>

<p>&nbsp;</p>

<p><strong>Reserve or Reserve Price</strong> &ndash; the minimum price below which the merchandise cannot be sold.</p>

<p>&nbsp;</p>

<p><strong>The Highest Bid &ndash;</strong> generally, the highest bid submitted in written form that the bidder wants to offer for the merchandise.</p>

<p>&nbsp;</p>

<p><strong>Aftersale Auction</strong> <strong>&ndash;</strong> the auction where unsold objects are offered at the reserve or lower price. The bidders can submit their proposals in written form, and the auction is won by the highest bid.</p>

<p>&nbsp;</p>

<p><strong>Online Auction</strong> &ndash; bidding not limited in time in which a proposal can be submitted at any time until the auction ends. If the bid is received during the last minute of the auction, the bidding time is extended for one minute until there are no more bidders. The auction is won by the highest bid.</p>

<p>&nbsp;</p>

<p><strong>Initial or Starting price</strong> &ndash; the price at which the auctioneer starts the calling the bids. If the starting price is above the reserve price, this means that the auctioneer has already received written requests for the lot.</p>

<p>&nbsp;</p>

<p><strong>Telephone Bid</strong> &ndash; as a rule, a written request which implies that the bidder will receive a telephone call during the auction so that he could submit a higher bid.</p>

<p>&nbsp;</p>

<p><strong>Absentee bid</strong> &ndash; written bid proposal for the merchandise submitted before the auction. The submitted requests affect the Starting price.</p>

<p>&nbsp;</p>

<p><strong>Hammer Price</strong> &ndash; the price at which the object of bidding is sold.</p>

<p>&nbsp;</p>

<p><strong>Electronic Sales Agent</strong> &ndash; a system which turns on when the bidder submits a higher bid than necessary for overbidding. For example, when the limit price or nearest best bid is 100&euro; and the bidder places an offer of 500&euro;, the automated system (electronic agent) counts all the bids up to 500&euro; inclusively in favour of the bidder.</p>

<p>&nbsp;</p>
    </div>
</div>

<br>
<br>

@endsection