@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container" style="min-height: 700px;">
    <div class="main" style="margin: 26px auto">
        <h1>Imprint/Disclaimer</h1>

        <p>&nbsp;</p>

<p>ACCORDING TO&nbsp;&sect; 5 TMG</p>

<p>Valery Kardanov<br />
An der Wurt 21<br />
28865 Lilienthal</p>

<p>&nbsp;</p>

<p>CONTACT</p>

<p>Telefon:&nbsp;0049 (0) 42986996454<br />
Telefax:&nbsp;0049 (0) 42986999864<br />
E-Mail: &nbsp;&nbsp;info@veryimportantlot.com</p>

<p>&nbsp;</p>

<p>SALES TAX ID</p>

<p>Value added tax identification number according to &sect;27a Sales Tax law:</p>

<p>&nbsp;</p>

<p>NOTICE ON EU DISPUTES</p>

<p>The European Commission provides a platform for online dispute resolution (OS):&nbsp;<a href="http://ec.europa.eu/consumers/odr">http://ec.europa.eu/consumers/odr</a><br />
Our email address can be found at the top of the imprint.</p>

<p>&nbsp;</p>

<p>DISCLAIMER</p>

<p>1. Responsibility for the content</p>

<p>As a service provider, we are responsible according to p. 7, paragraph 1 of TMG for the content of this site in accordance with the common law. However, according to p. 8-10 of TMG, as a service provider we are not obliged to monitor the transmitted or stored data, nor to conduct any investigation upon any presumably illegal activity. Thus, we assume no liability to suspend or to block access to the use of data required by conventional law. Performing the legal guarantees relating to such matter is still possible upon the receipt of information about a particular violation. As soon as we are notified of the violation in a due manner, we will immediately remove the content.</p>

<p>&nbsp;</p>

<p>2. Liability for External Links</p>

<p>Our site contains links to external websites, which are not subject to our influence or control in terms of their content for which we are not liable. The responsibility for the content of external sites always lies upon the owners or managers of these sites. External links are checked for possible rights violations at the moment of the redirection to an external site. At the redirection, the illegal contents may fail to be recognized. At the same time, content control of the external sites is not possible without a specific preliminary data on the violation. As soon as we are notified of the violation in a due manner, we will immediately remove the corresponding links.</p>

<p>&nbsp;</p>

<p>3. Copyright</p>

<p>Content composed by the site developers and all the subsequent work on these sites are subject to German copyright law. Copying, processing, distribution and other types of content use outside the copyright require written consent of its author or the creator. Downloading or copying of web pages is only allowed for private use, not for commercial use. To some extent, the content of the site is not compiled by the developer, so the third party copyright should also be taken into consideration. This applies especially to the content provided by any third parties. The users must pay attention to possible copyright infringements and we ask to report such cases. If we detect a copyright infringement, we will immediately remove the corresponding content from the site.</p>

<p>&nbsp;</p>

<p>USED IMAGES AND GRAPHICS</p>

<p><a href="https://ru.fotolia.com">https://ru.fotolia.com</a><br />
Alexandr Blinov, Paul Hill, kikkerdirk, Ralf Kalytta, ischoenrock</p>

<p>&nbsp;</p>
    </div>
</div>

<br>
<br>

@endsection