@extends('layouts.app')
@extends('layouts.nav')
@extends('layouts.footer')

@section('content')
<div class="container" style="min-height: 700px;">
    <div class="main" style="margin: 26px auto">
        <h1>Terms of use</h1>

        <p><strong>of the &laquo;VERY IMPORTANT LOT&raquo; service</strong></p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><strong>1. Scope, Definitions</strong></p>

<p>&nbsp;</p>

<p>1.1. The present Terms of Use apply for the use of online portals (including incorporated software) of an individual entrepreneur Kardanov Valery, mailing address: An der Wurt 21, 28865 Lilienthal, Germany (hereinafter &ldquo;The Manager&rdquo;), provided on the websitewww.veryimportantlot.com&nbsp;(hereinafter, the &quot;VERY IMPORTANT LOT&quot; service). The inclusion of additional user-specified conditions is not allowed, unless stated otherwise.</p>

<p>1.2. The concept of &quot;User&quot; under these Terms is used in general sense and it implies registered members as well as unregistered guests. The User can be a consumer or an entrepreneur.</p>

<p>1.3. Registered Users have review access to the &quot;VERY IMPORTANT LOT&quot; service as well as access to certain technical functions.</p>

<p>1.4. Guests are Users who have review-only access to a certain part of the &ldquo;VERY IMPORTANT LOT&rdquo; service, they do not need to register.</p>

<p>1.5. A Customer under the present Terms of Use is defined as an individual who concludes an agreement, most often unrelated to any official or personal professional activity.</p>

<p>1.6. An Entrepreneur in the framework of the present Terms of Use is an individual or a legal entity or a legally capable partnership who at the moment of conclusion of an agreement is engaged in official or personal professional activities.</p>

<p>1.7. Auctioneers under the present Terms of Use are referred to as generally recognized state-accredited auction houses selling items to others via the &quot;VERY IMPORTANT LOT&quot; service. Auctioneers are also regarded as Users who register under their own names.</p>

<p>1.8. Gallerists under the present Terms of Use are referred to as registered Entrepreneur Users, who offer items from his or her own store through the &quot;VERY IMPORTANT LOT&quot; service, and receive income.</p>

<p>1.9. A Bidder under the present Terms of Use is referred to as a registered User, be it a customer or entrepreneur, who wants to purchase merchandise from the Auctioneer through the &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>1.10. Live Virtual Auctions under the present Terms of Use are referred to as auctions which, in accordance with paragraph 156 of the German Civil Code (BGB), imply that Bidders participate in the &quot;VERY IMPORTANT LOT&quot; auction using different means of communication (PC, smartphone or tablet).</p>

<p>1.11 Traditional Auctions under the present Terms of Use are referred to as auctions which, in accordance with paragraph 156 of the German Civil Code (BGB), imply that the Bidders submit their bids prior to the auction using different means of communication (PC, smartphone or tablet) allowed by &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>1.12 Online auctions under the present Terms of Use are referred to as auctions in which the Auctioneer offers items exclusively through the &quot;VERY IMPORTANT LOT&quot; service and thus the merchandise can be purchased by the Bidder exclusively through the &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>&nbsp;</p>

<p><strong>2. Managing Services</strong></p>

<p>&nbsp;</p>

<p>2.1. The Manager provides the &quot;VERY IMPORTANT LOT&quot; service along with an online-auction portal, which enable the User to create or withdraw user&rsquo;s profiles and bidding announcements, as well as communicate with other portal users and participate in the auction with the help of provided software. The &quot;VERY IMPORTANT LOT&quot; service allows for bidding to be held in the format of Traditional Auction. The sale announcements can only be made by Auctioneers and Gallerists.</p>

<p>2.2. The Manager is not a party to the contract concluded between two Users. The Manager also does not guarantee that the Users will actually conclude a valid contract of sale. The execution of the concluded contract between Users shall be of exclusive liability of the Users.</p>

<p>2.3. By using the &quot;VERY IMPORTANT LOT&quot; service, The Manager allows the User to access his website and to use the corresponding software, which is installed on the Manager&rsquo;s server. The detailed description of the features and technical specifications of the software can be found on the Manager&rsquo;s website. The opening and support of data transmission channel between the IT system and Manager&rsquo;s server is beyond the Manager&rsquo;s responsibility.</p>

<p>2.4. The &quot;VERY IMPORTANT LOT&quot; service is subject to the restriction of the right of disposal. The full execution of the right of disposal is technically impossible and therefore it cannot be provided to the User be the Manager. However, the Manager is to ensure the adequate operability of the service. Thus, any matters related to maintenance, security or capacity, as well as to the events which fall beyond the responsibility of the Manager (violations in public communication networks, power failures etc.) may lead to malfunctions or temporary service interruption. The Manager however assumes the responsibility to execute maintenance and mitigation procedures, if possible.</p>

<p>2.5. The software is being periodically updated by the Manager. Therefore, the User has at his disposal the latest version of the Manager&rsquo;s software. In order to use the service, the User does not require any specific software configuration.</p>

<p>&nbsp;</p>

<p><strong>3. Changes in work.</strong></p>

<p>&nbsp;</p>

<p>3.1. The Manager reserves the right to modify the offered services or to offer other services unless it is unreasonable for the User.</p>

<p>3.2. in Addition, the Manager reserves the right to modify the offered services or to offer other services in the following cases:</p>

<ul>
	<li>due to a change in the legislation;</li>
	<li>if required by a court decision or in order to implement the decision of the authorities;</li>
	<li>if the change is necessary to eliminate the existing lack of security;</li>
	<li>when changes benefit the User;</li>
	<li>when the necessary change is of technical or procedural character and providing the it does not have any significant impact upon the User.</li>
</ul>

<p>3.3. Changes with minor impact on the functions of the &quot;VERY IMPORTANT LOT&quot; service do not fall into the conditions of the present article. This mainly relates to the changes in the graphic design and in the functions placement.</p>

<p>&nbsp;</p>

<p><strong>4. Right of Use</strong></p>

<p>&nbsp;</p>

<p>4.1. Holders of Rights of Use are individuals, legal entities and partnerships. Individuals of limited liabilities (especially minors) can use the &quot;VERY IMPORTANT LOT&quot; service only with the consent of their legal representatives. The Manager reserves the right to request a written statement of consent from a legal representative prior to granting access to the &ldquo;VERY IMPORTANT LOT&rdquo; service.</p>

<p>4.2. Auctioneers set merchandise for auction through the &quot;VERY IMPORTANT LOT&quot; service only if they have official permission in accordance with section 34B of Trade Rules. The Manager reserves the right to request the corresponding certificates and permissions prior to granting access to the &ldquo;VERY IMPORTANT LOT&rdquo; service.</p>

<p>4.3. Gallerists can set merchandise for auction through the &quot;VERY IMPORTANT LOT&quot; service only they own an online store.</p>

<p>4.4. By accepting the present Terms, the User confirms that he or she respects all the above mentioned conditions concerning the Right of Use.</p>

<p>4.5. Registration of a legal entity or partnership must be made by authorized officials. All the members of partnership should be mentioned in the partnership documents.</p>

<p>4.6. Each User may register once. His or her right to use the &quot;VERY IMPORTANT LOT&quot; service is considered personal and is not transferable.</p>

<p>&nbsp;</p>

<p><strong>5. Registration.</strong></p>

<p>&nbsp;</p>

<p>5.1. A successful registration of the User or getting a read-only Guest Access to the service enforces a contract for Right of use of the &quot;VERY IMPORTANT LOT&quot; service between the Manager and the User in accordance with the present Terms of Use.</p>

<p>5.2. After filling the data in the provided online form, a User can register by clicking the corresponding button to complete the registration process. The submission the registration form represents the User&#39;s offer to conclude an agreement, which the Manager could accept, but not obliged to. Upon the receipt of the proposal, the Manager has five days to accept the offer of the User which is done through confirming the acceptance by e-mail or through activation of the User&rsquo;s account. If within the specified period the Manager does not accept the User&#39;s offer to conclude a contract, the proposal it is regarded as a rejected.</p>

<p>5.3. The Manager is storing the text of the contract. The User can request the text of the contract, but only through the site of the Manager.</p>

<p>5.4. Prior to submission of the contract, the User can make necessary changes in the text of the contract with the help of keyboard and mouse.</p>

<p>5.5. The contract languages are German, Russian and English.</p>

<p>5.6. E-mail address is used to identify the User. The user guarantees that this address is identical to the email address provided during the registration and that it is functional and capable of receiving all e-mails sent by the Manager. The User must set the spam filters correctly to ensure the proper delivery of all the e-mails from the Manager or from any third parties authorized for information distribution.</p>

<p>5.7. All the information provided by the User in the registration form is true and complete. The user guarantees that his personal information (including e-mail address) is kept up to date. The provided personal information will not be subject to any checks by the Manager.</p>

<p>&nbsp;</p>

<p><strong>6. Contracts between the Users</strong></p>

<p>&nbsp;</p>

<p>Through the &quot;VERY IMPORTANT LOT&quot; service, the goods depending on their category can be purchased through a&nbsp;Traditional Auction. In this respect, the following contract conclusion rules apply:</p>

<p>6.1. Traditional Auction contract conclusion</p>

<p>During the Traditional Auction, a contract between the bidder and the Auctioneer is concluded through the bid submission and the hammer blow at the highest bid. In this case, as the relevant conditions of the Auctioneer come into power, the bidder is notified as per the rules of Traditional Auction. The bidder submits a bid prior to the auction. The submitted bids are considered during the auction at the lot call.</p>

<p>The bidder may submit a request through the &quot;VERY IMPORTANT LOT&quot; service either electronically or by telephone.</p>

<p>6.1.1. Electronic Bid Submission.</p>

<p>With the electronic bid submission, the Bidder indicates the desired bid amount in the provided for this purpose drop-down menu and then clicks the &quot;Bid now&quot; button. In a new opened window, the Bidder receives a notification of the conditions of sale of the Auctioneer which the Bidder must accept by clicking the mouse button. Clicking the &ldquo;Confirm the bid&rdquo; button provides the submitted bid with a legally binding status. Further on, the bid information is transferred in the electronic form to the Auctioneer and until being accepted by the latter it is reflected in the User&rsquo;s account as &quot;Sent&quot;. When the Auctioneer accepts the submitted bid, the bid status changes to &quot;Confirmed&quot;. If the submitted bid is superior to the other bids or is equal in value to the requests sent to the Auctioneer from other Bidders, the submitted bid wins the auction at the last hammer blow of the Auctioneer.</p>

<p>6.1.2 Telephone Bid submission</p>

<p>Bidding by telephone is done in the same way as electronic bid submission, with only difference that the Bidder in this case adds his or her telephone number in the provided for this purpose text field so the Auctioneer could contact him or her. If the submitted electronic bid of the Bidder is overridden in the auction, the Auctioneer may contact the Bidder and to suggest raising the bid. In such case, the Bidder can submit a higher rate bid. The Manager thus needs to transmit the registered telephone number of the Bidder to the Auctioneer. However, the Manager does not guarantee that the Auctioneer will call the Bidder during the auction, and shall not bear any responsibility for that.</p>

<p>6.2. A contract between an interested party and a Gallerist is concluded by a direct request. To do so, the User has to click the &quot;Send request&quot; button. The request will be forwarded to the Gallerist by e-mail.</p>

<p>&nbsp;</p>

<p><strong>7. Responsibilities of the user, responsibilities for the content.</strong></p>

<p>&nbsp;</p>

<p>7.1. Each User is personally responsible for the content which he or she places on the &quot;VERY IMPORTANT LOT &quot; service, especially for its reliability and legal safety. The User guarantees that the provided data is true. The User guarantees that the provided data does not violate the present Terms of Use or current legislation. It is forbidden to publish the following:</p>

<ul>
	<li>any content protected by copyright without authorized permission (e.g. photographs, publication of which was not authorized be the photographer and/or depicted person);</li>
	<li>false statements of facts;</li>
	<li>content which is intended to harm or slender others;</li>
	<li>the content of racist, nationalistic, discriminatory or abusive nature;</li>
	<li>content harmful to minors;</li>
	<li>any propaganda of violence;</li>
	<li>links to sites which do not meet legal regulations or the present Terms of Use.</li>
</ul>

<p>7.2. The content posted through the &quot;VERY IMPORTANT LOT&quot; remains the liability of the User and it does not represent the Manager`s point of view. The Manager does not guarantee the verity of the published content. Each User decides whether to question the validity of the content.</p>

<p>7.3. The User is responsible for any activity carried out under his/ hers access credentials.</p>

<p>7.4. The User is obliged not to use the &quot;VERY IMPORTANT LOT&quot; service in a manner that negatively affects the availability of the portal (for example, uploading large files or spam). The Manager reserves the right to limit the size of uploaded material to ensure the availability of the portal. If necessary, the Manager will inform about the applicable limitations within the service description.</p>

<p>7.5. The User admits and accepts the responsibility for his or her right to use the contents of uploaded files. Particularly he or she is responsible for not violating the rights of third parties, especially copyrights, commercial and personal non-property rights, and of competition rights in the legislative field.</p>

<p>7.6. The User is responsible for the confidentiality of access data. He or she is responsible for the data not becoming available to third parties. The User must immediately inform the Manager in case of his or her access being used by third parties.</p>

<p>7.7. The Auctioneer and the Gallerist admits and accepts the responsibility for the right to sell the items that he or she offers through the &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>7.8. The Auctioneer cannot bid on an item that he or she offers through the &quot;VERY IMPORTANT LOT&quot; service. In addition, it is forbidden to authorize third parties to submit a Bid for the items to raise the price on this item.</p>

<p>7.9. The Auctioneer is responsible for publishing the results of the Bidding by publishing the items&rsquo; list with the final prices in an anonymous form. The data is hosted in a provided for this purpose online database of the Manager. The link: www.veryimportantlot.com/archiv.</p>

<p>To do this, the Auctioneer posts the Bidding results to the online database of the Manager after the Bidding is finished by clicking &quot;Add selling price&quot; in his or her personal account of the &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>&nbsp;</p>

<p><strong>8. The rights of the Bidder</strong></p>

<p>&nbsp;</p>

<p>8.1. If the User violates the law or the present Terms of Use, or the Manager has any evidence of such violation, the Manager reserves the right to do the following without any notice or subsequent investigation:</p>

<ul>
	<li>terminate the User&rsquo;s account,</li>
	<li>make changes to the account changes as per the Rights of the Developer according to p. 9,</li>
	<li>issue a warning,</li>
	<li>block the User&#39;s access temporarily or permanently,</li>
	<li>take any necessary and appropriate measures.</li>
</ul>

<p>These measures rest entirely to the Manager&rsquo;s discretion.</p>

<p>8.2. If the User is blocked, he or she can no longer use the &quot;VERY IMPORTANT LOT&quot; service or register again.</p>

<p>8.3. In case of violation of the law or of the present Terms by the User, the Manager reserves the right to take legal actions against the User.</p>

<p><br />
<strong>9. </strong><strong>Concessions in the granting of Rights of Use in respect of the Manager</strong></p>

<p>&nbsp;</p>

<p>9.1. The Manager concludes a non-transferable and non-permanent contract with the User, defining the Right of Use, according to which the User gets access to the online portal and the corresponding software, provided by the Manager under the present Terms of Use.</p>

<p>9.2. Without written consent of the Manager, the User has no right to transfer his access to a third party. The User must ensure that he or she did not provide a third party with a possibility to get an unauthorized access to the &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>9.3. In the case of violation of the agreed Rights of Use, the Manager may prematurely terminate the contract on granting the Right of Use and thus to deny the User access to the &quot;VERY IMPORTANT LOT&quot; service. Usually, these actions are preceded by an official warning from the Manager which specifies the applicable terms.</p>

<p>9.4. Other legal and contractual provisions remain unchanged.</p>

<p>&nbsp;</p>

<p><strong>10. Concessions in the granting of Rights of Use in respect of the User</strong></p>

<p>&nbsp;</p>

<p>The Manager reserves the right for use of the content and information posted by the User through the &quot;VERY IMPORTANT LOT&quot; service or to transfer it to the third parties. The User grants the Manager an exclusive and not limited to the contract duration time Right of Use for the published content which implies the exclusive copyright allowing the Manager to process, to storage, to copy and to publish the content. The User admits the validity of the above mentioned concessions in respect to the Right of Use of the content.</p>

<p>&nbsp;</p>

<p><strong>11. Payments</strong></p>

<p>&nbsp;</p>

<p>The Bidder may pay for the merchandise purchased through the &quot;VERY IMPORTANT LOT&quot; service directly to the Auctioneer. In this case, the payments terms are unique for any particular Auctioneer and they are imposed by the latter.</p>

<p>&nbsp;</p>

<p><strong>12. Fees</strong></p>

<p>&nbsp;</p>

<p>12.1. The Bidder and the Gallerist can use the basic functions of the &quot;VERY IMPORTANT LOT&quot; service for free. The acquisition of additional rights for certain services is offered at a lump-sum price and is negotiated separately.</p>

<p>12.2. Auctioneer have to pay a fee to The Manager for the goods offered through &quot;VERY IMPORTANT LOT&quot;. The amount of the fee depends on the type of the chosen category and the number of the created articles. The remuneration is due when the auctioneer publishes his goods via &quot;VERY IMPORTANT LOT&quot; service.</p>

<p>&nbsp;</p>

<p><strong>13. Liability exemption</strong></p>

<p>&nbsp;</p>

<p>The User exempts the Manager from all claims that other Users or third parties may present to the Manager in case of any violation of their rights in respect to the content published by the User or in other cases concerning the use of the &quot;VERY IMPORTANT LOT&quot; service. The User thus assumes all the costs of any necessary legal actions including all applicable court and attorney costs. This provision shall not apply if the violation of the rights falls beyond the User&rsquo;s liability. In the event of a claim from a third party, the User shall immediately provide the Manager with accurate and complete information required for the claim verification and for legal defence.</p>

<p>&nbsp;</p>

<p><strong>14. Material liability of the Manager</strong></p>

<p>&nbsp;</p>

<p>The Manager is not liable for any damage which the Bidder may suffer from in connection with the purchase of a real estate from the Auctioneer. Otherwise, the Manager is deemed responsible for all contractual, quasi-contractual and legal obligations and tort liabilities for costs and damages caused as follows</p>

<p>14.1. The Manager has unlimited legal liability in case of:</p>

<ul>
	<li>willful misconduct or reckless disregard,</li>
	<li>any harm to life, physical or mental health caused by intentionally or by negligence,</li>
	<li>agreements on guarantees, unless otherwise specified,</li>
	<li>administrative responsibility in accordance with the law on liability for product quality.</li>
</ul>

<p>14.2. If the Manager violates essential contractual obligations by negligence, his liability is limited to the amount of the foreseeable damage, unless it falls under the provision of the preceding paragraph of unlimited liability. Essential contractual obligations are the obligations which the contract imposes on the Manager in respect of the objectives of the contract according to its content. The implementation of the Manager&rsquo;s obligations is intended for proper execution of the contract which meets the legitimate expectations of the User.</p>

<p>14.3. In all other cases, the Manager is exempt from any liability.</p>

<p>14.4. The liability exemptions stated above also apply to the Manager&rsquo;s scope of liability in regards of the actions of his assistants and legal representatives.</p>

<p>&nbsp;</p>

<p><strong>15. Duration, termination of the contract</strong></p>

<p>&nbsp;</p>

<p>15.1. The contract of Right of Use is concluded for an indefinite period and may at any time be unilaterally terminated by the User without complying with the time for termination of the contract and by the Manager with an obligatory two-week termination notice.</p>

<p>15.2. The right for an extraordinary contract termination due to any valid reason remains unchanged.</p>

<p>A reason is deemed valid if it implies a situation in which, taking into consideration all the circumstances of any individual case and the mutual interests of the parties, the continuation of the contractual relationships until the end of the agreed period or before the expiration of the two-week notification period is not possible.</p>

<p>In particular, valid reasons are:</p>

<ul>
	<li>significant violation of the general conditions of trade contracts that has not been fixed within the reasonable time period, or repetitive violations which make the continuation of the contractual relations impossible or unacceptable;</li>
	<li>elimination of one of the parties from the commercial register;</li>
	<li>re-registration attempt during current lockout as per the paragraph 7.1;</li>
	<li>If the common property of the other party or a substantial part of such property being subject to executive proceedings, or if there are any reasons for launching bankruptcy proceedings against the other party, or if the bankruptcy proceedings are rejected due to the lack of the residual property, or in case if the other party&rsquo;s property falls subject to a submitted Declaration of bankruptcy proceedings, an assertion or affidavit in respect of this property.</li>
</ul>

<p>15.3. The request for contract termination is submitted either in written or printed form (e.g. by email).</p>

<p>&nbsp;</p>

<p><strong>16. Terms of Use changes</strong></p>

<p>&nbsp;</p>

<p>16.1. The Manager reserves the right to implement changes to the present Terms of Use at any time with no explanation, unless this is unreasonable for the User. The Manager notifies the User about the change of Terms of Use in writing. If within four weeks after the notification the User expresses no objection to the new Terms of Use, the Terms are considered as accepted by the User. The notification to be sent by the Manager specifies the User&rsquo;s right for objection indicating the deadlines for submission of objections. If the User submits an objection to the changes within the specified deadline, the contractual relationship continues to exist in its original form.</p>

<p>16.2. The Manager also reserves the right to change the Terms of Use in the following cases:</p>

<ul>
	<li>if he or she is obliged to do so in connection with legislation changes;</li>
	<li>if required by a court decision or in order to implement the decision of the authorities;</li>
	<li>in case of implementation of completely new features, services or service elements which require detailed description in the Terms of Use, unless the original Terms of Use meet necessary requirements;</li>
	<li>if the changes are to benefit the User; &nbsp;</li>
	<li>if the changes are purely technical or procedural in their nature and have no significant impact on the User.</li>
</ul>

<p>16.3. The User&#39;s Rights for contract termination as per the paragraph 15 remains unchanged.</p>

<p>&nbsp;</p>

<p><strong>17. Final conditions and statements</strong></p>

<p>&nbsp;</p>

<p>17.1. The Legislation of the Federal Republic of Germany is applied to all legal relations of the parties. This legislation allows the Consumer to retain the right for protection, which may not be necessarily provided by the legislation of the country of residence.</p>

<p>17.2. If the User is an entrepreneur, a legal entity under public law or an owner of property which is subject to public law regulations registered on the territory of the Federal Republic of Germany, all disputes concerning the contract fall under the exclusive jurisdiction of the location of the Manager&rsquo;s enterprise. If the User is not on the territory of the Federal Republic of Germany, all disputes concerning the contract shall fall within the exclusive jurisdiction of the location of the Manager&rsquo;s enterprise, if the contract or contract requirements can be attributed to the professional or commercial activity of the User. However, in the above cases, the Manager reserves the right to apply to the court at the location of the User.</p>
    </div>
</div>
<br>
<br>

@endsection