<?php

return [
    'home' => 'home',
    'auctions' => 'auctions',
    'results' => 'results',
    'galleries' => 'galleries',
    'log_in' => 'Log in',
    'register' => 'Registation',
    'auction_catalogs' => 'Auction catalogs',
    'all_lots' => 'All current lots',
    'all_galleries' => 'All galleries',
    'lang' => 'Language',
    'log_out' => 'log out',
    'contact' => 'Contact',
    'partners' => 'For partners',
    'information' => 'Information',
    'password' => 'Password',
    'remember' => 'Remember&nbsp;me',
    'forgot' => 'Forgot Your Password?',
    'name' => 'Name',
    'last_name' => 'Last name',
    'address' => 'Address',
    'postal_code' => 'Postal code',
    'city' => 'City',
    'country' => 'Country',
    'confirm_password' => 'Confirm password',
    
    'reg_auction' => 'Registration auction house',
    'reg_gallery' => 'Registation gallery',
    'terms' => 'Terms of use',
    'imprint' => 'Imprint/Disclaimer',
    'privacy' => 'Privacy policy',
    'glossary' => 'Glossary',
    'right' => 'All rights reserved',
    'contact_person' => 'Contact person',
    'phone' => 'Phone',
    'web' => 'Website',
    'name_auction' => 'The name of the Auction House',
    'name_gallery' => 'The name of the Gallery'
];