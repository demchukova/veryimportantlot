<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lot_image', function (Blueprint $table) {
            $table->string('link_to_image')->default("lot_image_default.png");
            $table->integer('id_lot')->unsigned();
            $table->timestamps();
            $table->foreign('id_lot')->references('id')->on('lot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lot_image');
    }
}
