<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_lot')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->foreign('id_lot')->references('id')->on('lot');
            $table->foreign('id_user')->references('id')->on('all_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorite_lots');
    }
}
