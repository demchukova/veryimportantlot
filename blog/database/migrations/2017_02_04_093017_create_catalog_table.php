<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalog', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('datehour');
            $table->date('exibition_start');
            $table->date('exibition_end');
            $table->text('conditions');
            $table->string('bg_image')->default('catalog_default_bg_image.jpg');
            $table->string('image')->default('catalog_default_image.png');
            $table->integer('id_auction_house')->unsigned();
            $table->timestamps();
            $table->foreign('id_auction_house')->references('id')->on('auction_house');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalog');
    }
}
