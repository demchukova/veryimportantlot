<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorite_catalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_catalog')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->foreign('id_catalog')->references('id')->on('catalog');
            $table->foreign('id_user')->references('id')->on('all_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorite_catalogs');
    }
}
