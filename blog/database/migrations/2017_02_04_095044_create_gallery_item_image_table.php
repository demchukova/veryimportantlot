<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryItemImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_item_image', function (Blueprint $table) {
            $table->string('link_to_image')->default("gallery_items_default_image.png");
            $table->integer('logo')->default(0);
            $table->integer('id_gallery_item')->unsigned();
            $table->timestamps();
            $table->foreign('id_gallery_item')->references('id')->on('gallery_item');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_item_image');
    }
}
