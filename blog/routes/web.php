<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect' ]
    ],
    function()
    {
    Auth::routes();
    
    Route::get('localization/{locale}','LocalizationController@index');
    
    Route::get('/', 'PagesController@index');
    Route::get('/lot/{lotid}', 'PagesController@viewlot');
    Route::get('login', 'PagesController@login');
    Route::get('register', 'PagesController@register');
    Route::get('registerGallery', 'PagesController@registerGallery');
    Route::get('/registerAuctionHouse', 'PagesController@registerAuctionHouse');
    
    
    // ===== User Cabinet =====
    Route::get('usercabinet', 'PagesController@usercabinet');
    Route::get('usercabinet/search', 'PagesController@search');
    Route::get('usercabinet/searchGalleryItems', 'PagesController@searchGalleryItems');
    
    // ===== Gallery Cabinet =====
    Route::get('/gallerycabinet', 'PagesController@gallerycabinet');
    Route::get('gallerycabinet/search', 'PagesController@search');
    
    Route::get('usercabinet/searchGalleryItems', 'PagesController@searchGalleryItems');
    
    // ===== Favorites ========
    Route::get('/usercabinet/calendar', 'PagesController@calendar');
    Route::get('/usercabinet/favorites', 'PagesController@favorites');
    Route::get('/usercabinet/favoritecatalogs', 'PagesController@favoritecatalogs');
    Route::get('/usercabinet/favoritegalleryitems', 'PagesController@favoritegalleryitems');
    Route::get('/usercabinet/favoritegalleries', 'PagesController@favoritegalleries');
    Route::get('/usercabinet/favorites/delete/{lotid}', 'PagesController@favoritelotsdelete');
    Route::get('/usercabinet/favoritescatalogs/delete/{lotid}', 'PagesController@favoritescatalogsdelete');
    Route::get('/usercabinet/favoritegalleryitems/delete/{lotid}', 'PagesController@favoritegalleryitemsdelete');
    Route::get('/usercabinet/favoritegalleries/delete/{lotid}', 'PagesController@favoritegalleriesdelete');
    
    Route::get('usercabinet/bids', 'PagesController@bids');
    Route::get('usercabinet/pastbids', 'PagesController@pastbids');
    Route::get('usercabinet/bids/delete/{lotid}', 'PagesController@bidsdelete');
    Route::get('/addtofavoritecatalog/{catid}', 'CatalogController@addtofavoritecatalog');
    Route::get('/addtofavoritelots/{catid}', 'LotController@addtofavoritelots');
    
    // ===== Auction House Cabinet =====
    Route::get('auctioncabinet', 'AuctionHouseController@auctioncabinet');
    Route::get('auctioncabinet/search', 'AuctionHouseController@search');
    Route::get('auctioncabinet/catalog', 'AuctionHouseController@catalog');
    Route::get('auctioncabinet/catalog/create', 'CatalogController@create');
    Route::get('auctioncabinet/catalog/show/{id}', 'CatalogController@show');
    Route::get('auctioncabinet/catalog/editcatalog/{id}', 'AuctionHouseController@editCatalog');
    Route::get('/auctioncabinet/lot/{id}', 'AuctionHouseController@addlot');        
    Route::get('/auctioncabinet/catalog/deletecatalog/{id}','AuctionHouseController@deleteCatalog');
    Route::get('auctioncabinet/lot/edit/{id}', 'LotController@edit');
    Route::get('/auctioncabinet/catalog/delete/lot/{id}','LotController@deleteLot');
    Route::get('/auctioncabinet/lot/update/{id}','LotController@update');
    Route::get('auctioncabinet/bids', 'AuctionHouseController@bids');
    Route::get('auctioncabinet/bids/delete/{lotid}', 'AuctionHouseController@delete');
    
    // ===== static pages =====
    Route::get('/wiki/8', 'PagesController@terms');
    Route::get('/wiki/9', 'PagesController@disclaimer');
    Route::get('/wiki/10', 'PagesController@policy');
    Route::get('/wiki/11', 'PagesController@glossary');
    Route::get('/auction/catalog-list', 'PagesController@auction_catalog_list');
    Route::get('/auction/lot-list', 'PagesController@auction_lot_list');
    Route::get('/auction/catalog-archive-list', 'PagesController@auction_catalog_archive_list');
    Route::get('/auction/lot-archive-list', 'PagesController@auction_lot_archive_list');
    Route::get('/gallery/gallery-list', 'PagesController@gallery_list');
    Route::get('/lot-gallery/list', 'PagesController@lot_gallery_list');
    
    
    Route::get('checkEmail/{email}', function($em){
        $response = DB::table('users')->select('verified')->where('email', '=', $em)->get();
        return $response;
    });
    
    Route::get('/home', 'HomeController@index');
    Route::get('register/verify/{token}', 'Auth\RegisterController@verify');
    
    Route::get('admin', 'AdminController@index');
    
    Route::get('admin/galleries', 'GalleriesController@index');
    Route::get('admin/galleries/{id}', 'GalleriesController@store');
    
    Route::get('admin/auction_house', 'AuctionHouseController@index');
    Route::get('gallery/add', 'GalleriesController@additems');
    Route::get('gallery/items', 'GalleriesController@showall');
    
    Route::get('admin/auction_house/{id}', 'AuctionHouseController@store');
    
    });///все роуты ПОСТ ставьте ниже этой скобки, если случайно какой-то нужный роут удалился - не паникуйте а запишите ег о на мес
    
    Route::get('auctioncabinet/catalog/updatecatalog/{id}', 'AuctionHouseController@updateCatalog');
    Route::post('auctioncabinet/lot/add/{id}', 'AuctionHouseController@addlotincatalog');
    Route::post('/savePersonalInfo', 'UsersController@store');
    Route::get('/auctioncabinet/lot', 'AuctionHouseController@lot');
    Route::post('/registerGallery', 'RegisterGalleryController@register');
    Route::post('/registerAuctionHouse', 'RegisterAuctionController@register');
    Route::post('updateAuctionHouse', 'AuctionHouseController@update');
    Route::post('auctioncabinet/catalog/add', 'CatalogController@store');
    Route::post('/lot/makebid', 'LotController@makebid');

//GalleryPages
Route::get('/gallery/add', 'GalleriesController@additems');
Route::post('/gallery/add', 'GalleriesController@publish');
Route::get('/gallery/items', 'GalleriesController@showall');
Route::get('/gallery/{id}', 'GalleriesController@showall');
Route::get('/lot-gallery/{id}', 'GalleriesController@view');
Route::get('/lot-gallery/list', 'GalleriesController@allitems');
Route::post('/gallery/image/add/{id}', 'GalleriesController@setimg');
Route::post('/gallery/changes_bg', 'GalleriesController@change_bg');
Route::post('/gallery/changes_logo', 'GalleriesController@change_logo');
