<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    protected $table = 'lot';
    protected $fillable = [
        'name',
        'description',
        'minimum_bid'
    ];
    
}
