<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use DB;

class All_user extends Model
{
    protected $guarded = [];
    protected $table = 'all_users';
    
}