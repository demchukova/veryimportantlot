<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lot_image extends Model
{
    protected $table = 'lot_image';
    protected $fillable = [
        'link_to_image',
        'id_lot'
    ];
}
