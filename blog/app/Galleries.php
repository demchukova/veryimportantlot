<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galleries extends Model
{
    protected $guarded = [];
    protected $table = 'galleries';
}
