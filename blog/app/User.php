<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
        'email', 
        'password', 
        'email_token',
        'role'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        //'remember_token',
    ];
    
    
    public function verified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();
    }
    
    public function isAdmin()
    {
        return $this->admin; 
    }
    
}
