<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SimpleUser extends Model
{
  protected $table = "users" ;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'password', 
        'first_name',
        'last_name',
        'address',
        'postal_code',
        'city',
        'country',
        'verified',
        'email_token',
        'id_user'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    public function verified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();
    }
    
    public function isAdmin()
    {
        return $this->admin; 
    }   
    



}
