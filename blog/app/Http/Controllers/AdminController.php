<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


use Illuminate\Http\Request;
use App\Galleries;
use App\AuctionHouse;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function isAdmin(int $id)
    {
        
        $is_admin =  DB::table('users')->select('id', 'role', 'admin')->where('id', '=', $id)->get();
         if ($is_admin[0]->role == 1 && $is_admin[0]->admin == 1)
        return 1;
        else return 0;
        
        
    }
    
    public function index()
    {
        if (Auth::check())
        {
            if ($is_admin = $this->isAdmin(Auth::id()))
            return view('admin/admin');
        }
        return redirect('/home');
    }
    
}
