<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\EmailVerification;

use Mail;

use Illuminate\Http\Request;
use App\Galleries;
use App\User;
use Illuminate\Support\Facades\Input;

class GalleriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'showall', 'view', 'allitems']);
    }
    
    public function isAdmin(int $id)
    {
        
        $is_admin =  DB::table('users')->select('id', 'role', 'admin')->where('id', '=', $id)->get();
         if ($is_admin[0]->role == 1 && $is_admin[0]->admin == 1)
        return 1;
        else return 0;
    }
    public function index()
    {
        if (Auth::check())
        {
            if ($is_admin = $this->isAdmin(Auth::id()))
                return view('admin.galleries');
        }
        return redirect('/home');
    }
    
    public function store(Request $request, String $id_user)
    {
        if (Auth::check())
        {   
            $is_admin = $this->isAdmin(Auth::id());
            if ($is_admin == 1)
            {
         
             $data = $request->except('_token', '_csrf-frontend');
            
             $gallerie =  User::findOrFail($id_user);
             Mail::send('emails.confirm', ['gallerie' => $gallerie], function ($m) use ($gallerie) {
                $m->from('support@veryimportantlot.com', 'Very Important Lot');
    
                $m->to($gallerie->email, $gallerie->name)->subject('Registration Confirmed');
            });

            DB::statement("UPDATE `users` SET `verified` = '1' where `id` = '$id_user'");
            return view('admin.galleries');
            }
        }
        return redirect('/home');
    }
    
    public function showall($id_gall)
    {
        $gallery = DB::table('galleries')->select('*')->where('id', '=', $id_gall)->first();
        $gallery_items = DB::table('gallery_item')->select('*')->where('id_gallery', '=', $id_gall)->get();
        $images = DB::table('gallery_item_image')->select('*')->get();
        $gallery_image = DB::table('gallery_image')->select('link_to_image')->where('id_gallery', '=', $gallery->id)->latest()->get();
        $logo = DB::table('gallery_item_image')->select('link_to_image')->where('logo', '=', 1)->where('id_gallery_item', '=', $gallery->id)->latest()->get();
        return view('gallery.items', compact('gallery_image', 'logo', 'gallery', 'gallery_items', 'images'));
    }
    
    public function additems()
    {
        if (Auth::user()->role != 2)
            return redirect('/');
        else
        return view('gallery.add');
    }
    
    public function publish()
    {
        $id_gall = DB::table('galleries')->select('id')->where('id_user', '=', Auth::user()->id)->first();
        
       // dd($id_gall->id);
        $this->validate(request(), [
                'name' => 'required',
                'price' => 'required',  //|min:2
                'description' => 'required|min:10',
                'file' => 'required|file'//|max:10
            ]);
        
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();
        $path = public_path('/img/');
        $file->move($path,$filename);
        
        DB::table('gallery_item')->insert([
            'name' => request('name'),
            'price' => request('price'),
            'description' => request('description'),
            'id_gallery' => $id_gall->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        
        $id_item = DB::table('gallery_item')->select('id')
                ->where('name', '=', request('name'))
                ->where('price', '=', request('price'))
                ->where('description', '=', request('description'))
                ->orderBy('updated_at', 'desc')->first();

        
        DB::table('gallery_item_image')->insert([
            'link_to_image' => $filename,
            'id_gallery_item' => $id_item->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
            
        return redirect('/gallerycabinet');
    }
    
    public function view($id_item)
    {
        $item = DB::table('gallery_item')->select('*')->where('id', '=', $id_item)->first();
        $gallery = DB::table('galleries')->select('*')->where('id', '=', $item->id_gallery)->first();
        $user = DB::table('users')->where('id', '=', $gallery->id_user)->first();
        $item_image = DB::table('gallery_item_image')->select('*')->where('id_gallery_item', '=', $item->id)->get();
        $logo = DB::table('gallery_item_image')->select('link_to_image')->where('logo', '=', 1)->where('id_gallery_item', '=', $gallery->id)->latest()->get();
        return view('gallery.item', compact('gallery', 'item', 'item_image', 'user', 'id_item', 'logo'));
    }
    
    public function allitems()
    {
        $gallery = DB::table('galleries')->select('*')->get();
        $gallery_items = DB::table('gallery_item')->select('*')->get();
        $images = DB::table('gallery_item_image')->select('*')->get();
        return view('gallery.allitems', compact('gallery', 'gallery_items', 'images'));
    }
    
    public function setimg($id_item)
    {
        $id_gall = DB::table('galleries')->select('id')->where('id_user', '=', Auth::user()->id)->first();
        
       // dd($id_gall->id);
        $this->validate(request(), [
                'file' => 'required|file'//|max:10
            ]);
        
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();
        $path = public_path('/img/');
        $file->move($path,$filename);

        
        DB::table('gallery_item_image')->insert([
            'link_to_image' => $filename,
            'id_gallery_item' => $id_item,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        
        return redirect('/lot-gallery/'.$id_item);
    }
    
    public function change_bg()
    {
        $id_gall = DB::table('galleries')->select('id')->where('id_user', '=', Auth::user()->id)->first();
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();
        $path = public_path('/img/');
        $file->move($path,$filename);
        
        //dd($id_gall);
        DB::table('gallery_image')->insert([
            'link_to_image' => $filename,
            'id_gallery' => $id_gall->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        
        return redirect('/gallerycabinet');
    }
    
    public function change_logo()
    {
        $id_gall = DB::table('galleries')->select('id')->where('id_user', '=', Auth::user()->id)->first();
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();
        $path = public_path('/img/');
        $file->move($path,$filename);
        
        //dd($id_gall);
        DB::table('gallery_item_image')->insert([
            'link_to_image' => $filename,
            'logo' => 1,
            'id_gallery_item' => $id_gall->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
            ]);
        
        return redirect('/gallerycabinet');
    }
}
