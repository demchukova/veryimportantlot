<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    public function index()
    {
        $nav = ["active","","",""];
        return view('home', compact('nav'));
    }
    
    public function viewlot($lotid)
    {
        return view('lotpage', compact('lotid'));
    }
    
    public function login()
    {
        return view('auth.login');
    }
    
    public function register()
    {
        return view('auth.register');
    }
    
    public function registerGallery()
    {
        return view('auth.registerGallery');
    } 
    public function registerAuctionHouse()
    {
        return view('auth.registerAuctionHouse');
    }

    public function checkEmail()
    {
        return view('auth.checkEmail');
    }

    public function terms()
    {
        return view('terms');
    }
    
    public function policy()
    {
        return view('policy');
    }
    
    public function disclaimer()
    {
        return view('disclaimer');
    }
    
    public function glossary()
    {
        return view('glossary');
    } 
    
    public function bidsdelete($lotid)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "cabActive";
        $setingsClass = "";
        $active = "activeBid";
        $past = "";
        DB::statement("DELETE FROM `rates` WHERE `id` = '$lotid'");
        return view('usercabinet.bids', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "active", 'past'));
    } 
    
    public function isGallery(int $id)
    {
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
        if ($is_auch[0]->role == 2)
            return 1;
        else
            return 0;
    }
    
    public function isAuctionHouse(int $id)
    {
        
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_auch[0]->role == 3)
        return 1;
        else return 0;
    }
    
    public function isUser(int $id)
    {
        return (!$this->isAuctionHouse($id) && !$this->isGallery($id));
    }
    
    
    public function usercabinet(Request $request)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            return view('usercabinet', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass'));
        }
        else
            return redirect('/home');
    }
    
    public function gallerycabinet(Request $request)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check() && $this->isGallery(Auth::id()))
        {
            $id = DB::table('users')->select('id')->where('role', '=', '2')->where('id', '=', Auth::id())->get();
            
            if($id[0]->id == Auth::id())
                return view('gallerycabinet.index', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass'));
            else
                return redirect('/');
        }
        else
            return redirect('/');
    }
    
    public function search()
    {
        $searchClass = "cabActive";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "";
        $page = ["active",""];
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.search', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', 'page'));
        else
            return redirect('/home');
    }
    
    public function searchGalleryItems()
    {
        $searchClass = "cabActive";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "";
        $page = ["","active"];
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            return view('usercabinet.search', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', 'page'));
        }
        else
            return redirect('/home'); 
    }
    
    public function calendar()
    {
        $searchClass = "";
        $calendarClass = "cabActive";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            return view('usercabinet.calendar', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass'));
        }
        else
            return redirect('/home'); 
    }
    
    public function favoritelotsdelete($lotid)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["activeBid","","",""];
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            DB::statement("DELETE FROM `favorite_lots` WHERE `id` = '$lotid'");
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        }
        else
            return redirect('/home'); 
    }
    
    public function favoritescatalogsdelete($lotid)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","activeBid","",""];
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            DB::statement("DELETE FROM `favorite_catalogs` WHERE `id` = '$lotid'");
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        }
        else
            return redirect('/home'); 
    }
    
    public function favoritegalleryitemsdelete($lotid)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","","activeBid",""];
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            DB::statement("DELETE FROM `favorite_gallery_item` WHERE `id` = '$lotid'");
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        }
        else
            return redirect('/home'); 
    }
     
    public function favoritegalleriesdelete($lotid)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","","","activeBid"];
        if (Auth::check() && $this->isUser(Auth::id()))
        {
            DB::statement("DELETE FROM `favorite_galleries` WHERE `id` = '$lotid'");
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav")); 
        }
        else
            return redirect('/home'); 
    }
    
    public function favorites()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["activeBid","","",""];
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        else
            return redirect('/home'); 
    }
    
    public function favoritecatalogs()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","activeBid","",""];
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        else
            return redirect('/home'); 
    }
    
    public function favoritegalleryitems()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","","activeBid",""];
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        else
            return redirect('/home');
    }
    
    public function favoritegalleries()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        $fav = ["","","","activeBid"];
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.favorites', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "fav"));
        else
            return redirect('/home');
    }
    
    public function bids()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "cabActive";
        $setingsClass = "";
        $active = "activeBid";
        $past = "";
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.bids', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "active", 'past'));
        else
            return redirect('/home');
    }
    
    public function pastbids()
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "cabActive";
        $setingsClass = "";
        $active = "";
        $past = "activeBid";
        if (Auth::check() && $this->isUser(Auth::id()))
            return view('usercabinet.bids', compact('searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass', "active", 'past'));
        else
            return redirect('/home');
    }
    
    public function auction_catalog_list()
    {
        $page = ['active', ''];
        $nav = ["","active","",""];
        return view('auction.catalog-list', compact('page', 'nav'));
    }
    
    public function auction_lot_list()
    {
        $page = ['', 'active'];
        $nav = ["","active","",""];
        return view('auction.lot-list', compact('page','nav'));
    }
    
    public function auction_catalog_archive_list()
    {
        $page = ['active', ''];
        $nav = ["","","active",""];
        return view('auction.catalog-archive-list', compact('page','nav'));
    }
    
    public function auction_lot_archive_list()
    {
        $page = ['', 'active'];
        $nav = ["","","active",""];
        return view('auction.lot-archive-list', compact('page','nav'));
    }
    
    public function gallery_list()
    {
        $gallery = DB::table('users')->select('*')
                ->where('role', '=', '2')
                ->where('verified', '=', '1')
                ->join('galleries', 'galleries.id_user', '=', 'users.id')
                ->get();
        $nav = ["","","","active"];
        return view('gallery.gallery-list', compact('gallery','nav'));
    }
    
    public function lot_gallery_list()
    {
        $nav = ["","","","active"];
        return view('gallery.lot-gallery-list', compact('nav'));
    }
}