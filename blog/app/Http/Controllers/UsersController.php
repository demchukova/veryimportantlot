<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function saveInfo(Request $request)
    {
        
    }
    
     protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'address' => 'required|max:255',
            'zip_code' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
        ]);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $searchClass = "";
        $calendarClass = "";
        $favoritesClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        
        $data = $request->except('_token', '_csrf-frontend');
        $dbdata = DB::table('all_users')->where('id', '=', Auth::id())->first();
        $dbuser = DB::table('users')->where('id', '=', Auth::id())->first();
        $old_pass = $data['User']['old_password'];
        $data = $data['User'];
        if (strcmp($data['password'], '') != 0)
        {
            $retu = "Old password does not match!";
            if (Hash::check($old_pass, $dbuser->password))
            {
                if (strcmp($data['password'], $data['password_repeat']) == 0 && (strlen($data['password']) >= 6))
                {
                    $pass = Hash::make($data['password']);                
                    DB::statement("UPDATE `users` SET `password` = '$pass' WHERE `id` = '$dbdata->id'");
                    $retu = "Saved!";
                }
                else
                {
                    $retu = "New passwords does not match!";
                    return view('usercabinet', compact('retu', 'searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass'));
                }
            }
        }
        $valid = $this->validator($data);
        if ($valid->fails()) 
        {
            $retu = "Some of the fields does not match our requirements!";
        }
        else
        {
            $fn = $data['firstname'];
            $ln = $data['lastname'];
            $add = $data['address'];
            $zip = $data['zip_code'];
            $city = $data['city'];
            $country = $data['country'];
            
            DB::statement("UPDATE `all_users` SET `first_name` = '$fn', `last_name` = '$ln', `address` = '$add', `postal_code` = '$zip', `city` = '$city', `country` = '$country' WHERE `id` = '$dbdata->id'");
            $retu = "Saved!";
        }
        return view('usercabinet', compact('retu', 'searchClass', 'calendarClass', 'favoritesClass', 'bidsClass', 'setingsClass'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
