<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;
use Mail;
use App\Mail\EmailVerification;
use Validator;
use Illuminate\Support\Facades\Hash;

use App\Galleries;

class RegisterGalleryController extends Controller
{
        use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
      protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'nameGalery' => 'required|max:255',
            'contact' => 'required|max:255',
            'address' => 'required|max:255',
            'postal_code' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'phone_number' => 'required|max:255',
            'web_site' => 'required|max:255',
            'password' => 'required|min:6|confirmed'
                ]);
    }

    protected function afterCreate(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'email_token' => str_random(10),
            'role'=>'2'
        ]);
        
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   protected function create(array $data)
    {
        $id_user = DB::table('users')->max('id'); 
        Galleries::create([
            'name' => $data['nameGalery'],
            'contact_person' => $data['contact'],
            'address' => $data['address'],
            'city' => $data['city'],
            'postal_code' => $data['postal_code'],
            'country' => $data['country'],
            'phone_number' => $data['phone_number'],
            'website' => $data['web_site'],
            'id_user' => $id_user,
        ]);

        return (0);
    }

      public function register(Request $request)
     { 
        $validator = $this->validator($request->all());
        if ($validator->fails()) 
        {
            $this->throwValidationException($request, $validator);
        }

        DB::beginTransaction();
        try
        {
        
            $user = $this->afterCreate($request->all());
            $this ->create($request->all());
            Mail::send('emails.regist', ['user' => $user], function ($m) use ($user) {
            $m->from('support@veryimportantlot.com', 'Very Important Lot');
            $m->to($user->email)->subject('Registration');
        });
            $reteredUser = $user->email;
            DB::commit();
            return view('home', compact('reteredUser'));
        }
        catch(Exception $e)
        {
            DB::rollback(); 
            return back();
        }
    }

    public function verify($token)
    {
        User::where('email_token',$token)->firstOrFail()->verified();
        return redirect('login');
    }
}
