<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Catalog;
use Illuminate\Support\Facades\Input; 


use Illuminate\Http\Request;


class CatalogController extends Controller
{
    public function isAuctionHouse(int $id)
    {
        
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_auch[0]->role == 3)
        return 1;
        else return 0;
    }
    public function isUser(int $id)
    {
        
        $is_user =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_user[0]->role == 1)
        return 1;
        else return 0;
    }
     public function isGallery(int $id)
    {
        
        $is_gal =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_gal[0]->role == 3)
        return 1;
        else return 0;
    }
  
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addtofavoritecatalog($catid)
    {
        if (Auth::check())
        {
            if($this->isUser(Auth::id()))
            {
                $userdata = DB::table('all_users')->where('id_user', '=', Auth::id())->first();
                $userid = $userdata->id;
                
                $email = DB::table('users')->where('id', '=', Auth::id())->first();
                
                $f = DB::select("
                        SELECT COUNT(*) as 'nr' FROM `favorite_catalogs` WHERE `id_catalog` = '$catid' AND `id_user` = '$userid' ;
                    ");   
                $f = $f[0]->nr;
                if ($f > 0)
                {
                    DB::statement("DELETE FROM `favorite_catalogs` WHERE `id_catalog` = '$catid' AND `id_user` = '$userid';");
                }
                else
                {
                    DB::statement("INSERT INTO `favorite_catalogs` (id, `id_catalog`, `id_user`, `created_at`, `updated_at`) VALUES (NULL, '$catid', '$userid', (SELECT now()), (SELECT now()));");
                }
                return redirect('/auctioncabinet/catalog/show/'.$catid);
            }
            else if($this->isAuctionHouse(Auth::id()) && $this->isGallery(Auth::id()))
                return redirect('/auctioncabinet/catalog/show/'.$catid);
        }
        return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $searchClass = "cabActive";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
                return view('catalog.create_catalog',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        }
    
        return redirect('/');
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'datehour' => 'required|date',
            'exibition_start' => 'required|date',
            'exibition_end' => 'required|date',
            'conditions' => 'required'
           // 'bg_image' => 'required|mimes:png,jpeg,pdf,rar|max:1400',
           // 'image' => 'required|mimes:png,jpeg,pdf,rar|max:1400'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        
        $searchClass = "";
        $catalogClass = "cabActive";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
    
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $req->except('_token', '_csrf-frontend');
                $catalog = new Catalog;
               
                $dbuser = DB::table('auction_house')->select('id')->where('id_user', '=', Auth::id())->first();
                $name = $data['name'];
                $datahour = $data['datehour'];
                $exibition_start = $data['exibition_start'];
                $exibition_end = $data['exibition_end'];
                $condition = $data['conditions'];
                $id_auction_house = $dbuser->id;
                if ($req->hasFile('bg_image'))
                {
                    $file = Input::file('bg_image');
                    $filename = $file->getClientOriginalName();
                    $catalog->bg_image= $filename;
                    $path = public_path('/img/');
                    $file->move($path,$filename);
                }
                if ($req->hasFile('image'))
                {
                    $file = Input::file('image');
                    $filename = $file->getClientOriginalName();
                    $catalog->image = $filename;
                    $path = public_path('/img/');
                    $file->move($path,$filename);
                }
                $datanew = array('name'=>$name, 'datehour'=>$datahour,'exibition_start'=>$exibition_start, 'exibition_end'=>$exibition_end,'conditions'=>$condition, 'id_auction_house'=>$id_auction_house, 'bg_image'=>$catalog->bg_image, 'image'=>$catalog->image);
                
                
                $valid = $this->validator($datanew);
                if ($valid->fails()) 
                {
                    $retu = "Some of the fields does not match our requirements!";
                    return view('catalog.create_catalog',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass')); 
                }else{
                $addCatalog = DB::table('catalog')->insert($datanew);
                }
                // return view('auctioncabinet.catalog',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
                return redirect('auctioncabinet/catalog');
            }
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $req , String $id)
    {
        $searchClass = "cabActive";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        
        $data = $req->except('_token', '_csrf-frontend');
        
        $catalog = DB::table('catalog')->select('id')->where('id' , '=', $id)->first();
        return view('catalog.catalog_show',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass', 'catalog'));
    }

}
