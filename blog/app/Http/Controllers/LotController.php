<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\EmailVerification;
use Validator;
use Mail;
use App\AuctionHouse;
use App\User;
use App\Lot;
use App\Catalog;
use App\Lot_image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input; 

class LotController extends Controller
{
    
    public function isGallery(int $id)
    {
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
        if ($is_auch[0]->role == 2)
            return 1;
        else
            return 0;
    }
    
    public function isAuctionHouse(int $id)
    {
        
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
         if ($is_auch[0]->role == 3)
        return 1;
        else return 0;
    }
    
    public function isUser(int $id)
    {
        return (!$this->isAuctionHouse($id) && !$this->isGallery($id));
    }
    
    public function getAllUsersId($userid)
    {
        $uid = DB::table('all_users')->select('id')->where('id_user', '=', $userid)->first();
        return $uid->id;
    }
    
    public function addtofavoritelots($lotid)
    {
        if (Auth::check())
        {
            if ($this->isUser(Auth::id()))
            {
                $userid = $this->getAllUsersId(Auth::id());
                $f = DB::select("
                        SELECT COUNT(*) as 'nr' FROM `favorite_lots` WHERE `id_lot` = '$lotid' AND `id_user` = '$userid'; ;
                    ");   
                $f = $f[0]->nr;
                if ($f > 0)
                {
                    DB::statement("DELETE FROM `favorite_lots` WHERE `id_lot` = '$lotid' AND `id_user` = '$userid';");
                }
                else
                {
                    DB::statement("INSERT INTO `favorite_lots` (id, `id_lot`, `id_user`, `created_at`, `updated_at`) VALUES (NULL, '$lotid', '$userid', (SELECT now()), (SELECT now()));");
                }
            }
            return redirect()->back();
        }
        return redirect('/login');
    
    }
    
    public function makebid(Request $request)
    {
        if (Auth::check())
        {
            if ($this->isUser(Auth::id()))
            {
                $userid = $this->getAllUsersId(Auth::id());
                $bid = $request->except('_token');
                $lotid = $bid['lotid'];
                $bid = $bid['bid'];
    
                DB::statement("INSERT INTO `rates` (`id`, `id_user`, `id_lot`, `value`, `created_at`, `updated_at`) VALUES (NULL, '$userid', '$lotid', '$bid', (SELECT now()), (SELECT now()));");
            }
            return redirect()->back();
        }
        return redirect('/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $request->except('_token', '_csrf-frontend');
                $lot = Lot::find($id);
                return view('lot.lot_edit',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass', 'lot'));
            }
      }
       return redirect('/');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $request->except('_token', '_csrf-frontend');
                $lot = lot::find($id);
                $lot->name = $request->input('name');
                $lot->description = $request->input('description');
                $lot->minimum_bid = $request->input('minimum_bid');
                $lot->save();
                return redirect('auctioncabinet/catalog/');
            }
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteLot($id)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $lot = Lot::find($id);
                DB::table('lot_image')->select('id')->where('id_lot','=',$id)->delete();
                DB::table('lot')->select('id')->where('id','=',$id)->delete();
                return back();
            }
        }
        return redirect('/');
    }
}
