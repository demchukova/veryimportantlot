<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mail\EmailVerification;
use Validator;
use Response;
use Redirect;
use Session;
use Mail;
use App\AuctionHouse;
use App\User;
use App\Lot;
use App\Catalog;
use App\Lot_image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;

class AuctionHouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function isAdmin(int $id)
    {
        $is_admin =  DB::table('users')->select('id', 'role', 'admin')->where('id', '=', $id)->get();
        if ($is_admin[0]->role == 1 && $is_admin[0]->admin == 1)
            return 1;
        else
            return 0;
    }
    
    public function isAuctionHouse(int $id)
    {
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
        if ($is_auch[0]->role == 3)
            return 1;
        else
            return 0;
    }
    
    public function isGallery(int $id)
    {
        $is_auch =  DB::table('users')->select('id', 'role')->where('id', '=', $id)->get();
        if ($is_auch[0]->role == 2)
            return 1;
        else
            return 0;
    }
    
    public function index()   
    {   
     if (Auth::check())
        {
            if ($is_admin = $this->isAdmin(Auth::id()))
            return view('admin.auction_house');   
        }
        return redirect('/home');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, string $id)
    {
       if ((Auth::check()))
       {
            $is_admin = $this->isAdmin(Auth::id());
            if ($is_admin == 1)
            {
            
          $data = $request->except('_token', '_csrf-frontend');
            $auctionhouse = DB::table('auction_house')->where('id_user', '=', $id)->first();
            
            $users =  DB::table('users')->where('id', '=', $id)->first();
           
            Mail::send('emails.confirm', ['auctionhouse' => $auctionhouse], function ($m) use ($auctionhouse, $users) {
                $m->from('support@veryimportantlot.com', 'Very Important Lot');
                $m->to($users->email, $auctionhouse->name)->subject('Registration Confirmed');
            });
            DB::statement("UPDATE `users` SET `verified` = 1 where `id` = '$id'");
            return view('admin.auction_house');
            }
       }
       return redirect('/home');
    }
    
   
  
    public function addlot(string $id)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
                return view('catalog.addlot', compact('id','searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        }
        return redirect('/');
    }
    

    protected function validator_lot(array $data)

    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'description' => 'required',
            'minimum_bid' => 'required',
            'link_to_image' => 'required'
        ]);
    }
    
    public function addlotphoto(Request $request, int $lot_id)
    {
        if ($request->hasFile('link_to_image'))
        {
            $files = Input::file('link_to_image');
            foreach($files as $file)
            {
                $entry = new Lot_image;
                $rules = array('link_to_image' => 'required|mimes:jpeg,png,pdf,txt');
                $validator = Validator::make(array('file' => $file), $rules);
                $path = public_path('img/');
                $filename= $file->getClientOriginalName();
                $file = $file->move($path, $filename);
                $entry->id_lot = $lot_id;
                $entry->link_to_image = $filename;
                $entry->save();
            }
            
        }
        return 1;
    }    
    
    public function editCatalog(Request $request , String $id)
    {
        $searchClass = "";
        $catalogClass = "cabActive";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        
        $data = $request->except('_token', '_csrf-frontend');
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $catalog = Catalog::find($id);
                return view('catalog.catalog_edit',compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass', 'catalog'));
            }
        }
        return redirect('/');
    }
    
    protected function validatorCatalog(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'datehour' => 'required|date',
            'exibition_start' => 'required|date',
            'exibition_end' => 'required|date',
            'conditions' => 'required'
        ]);
    }
    
    public function updateCatalog(Request $request, $id)
    {
        $searchClass = "";
        $catalogClass = "cabActive";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $request->except('_token', '_csrf-frontend');
                $catalog = Catalog::find($id);
                $catalog->name = $request->input('name');
                $catalog->datehour = $request->input('datehour');
                $catalog->exibition_start = $request->input('exibition_start');
                $catalog->exibition_end = $request->input('exibition_end');
                $valid = $this->validatorCatalog($data);
                $catalog->save();
                return redirect('auctioncabinet/catalog/');
            }
        }
        return redirect('/');
    }
    
    public function deleteCatalog(Request $request, $id)
    {
        $searchClass = "";
        $catalogClass = "cabActive";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $request->except('_token', '_csrf-frontend');
                $catalog = Catalog::find($id);
                DB::table('catalog')->select('id')->where('id','=',$id)->delete();
                return redirect('auctioncabinet/catalog/');
            }
        } return redirect('/');
    }
    
    
    public function addlotincatalog(Request $request, String $id)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                $data = $request->except('_token', '_csrf-frontend');
                $dbuser = DB::table('catalog')->select('id')->where('id' ,'=', $id)->first();
                $name = $data['name'];
                $description = $data['description'];
                $minimum_bid = $data['minimum_bid'];
                $id_catalog = $dbuser->id;
                
                $datanew = array('name'=>$name, 'description'=>$description, 'minimum_bid'=>$minimum_bid, 'id_catalog'=>$id_catalog);
                
                
                $valid = $this->validator_lot($data);
                if ($valid->fails())
                {
                    $retu = "Some of the fields does not match our requirements!";
                    return view('catalog.addlot', compact('id', 'searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
                }
                else
                {
                    $addLot = DB::table('lot')->insert($datanew);
                    $lot_id = DB::table('lot')->select('id')->orderBy('id', 'desc')->first();
                    $this->addlotphoto($request, $lot_id->id);
                }
                return redirect('auctioncabinet/catalog');
            }
        }
        return redirect('/');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'contact_person' => 'required|max:255',
            'address' => 'required|max:255',
            'zip_code' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'phone_number' => 'required|max:255',
            'website' => 'required|max:255',
        ]);
    }

    public function update(Request $request)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {    
                $data = $request->except('_token', '_csrf-frontend');
                $dbuser = DB::table('users')->where('id', '=', Auth::id())->first();
                $old_pass = $data['User']['old_password'];
                $data = $data['User'];
                if (strcmp($data['password'], '') != 0)
                {
                    $retu = "Old password does not match!";
                    if (Hash::check($old_pass, $dbuser->password))
                    {
                        if (strcmp($data['password'], $data['password_repeat']) == 0 && (strlen($data['password']) >= 6))
                        {
                            $pass = Hash::make($data['password']);                
                            DB::statement("UPDATE `users` SET `password` = '$pass' WHERE `id` = '$dbuser->id'");
                            $retu = "Saved!";
                        }
                        else
                        {
                            $retu = "New passwords does not match!";
                            return view('auctioncabinet', compact('retu', 'searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
                        }
                    }
                }
                $valid = $this->validator($data);
                if ($valid->fails()) 
                {
                    $retu = "Some of the fields does not match our requirements!";
                }
                else
                {
                    $fn = $data['name'];
                    $ln = $data['contact_person'];
                    $add = $data['address'];
                    $zip = $data['zip_code'];
                    $city = $data['city'];
                    $country = $data['country'];
                    $phone_number = $data['phone_number'];
                    $website = $data['website'];
                    
                    DB::table('auction_house')
                    ->where('id_user', $dbuser->id)
                    ->update([  'name' => $fn,
                                'contact_person' => $ln,
                                'address' => $add,
                                'postal_code' => $zip,
                                'city' => $city,
                                'country' => $country,
                                'phone_number' => $phone_number,
                                'website' => $website,
                    ]);
                    $retu = "Saved!";
                }
                return view('auctioncabinet', compact('retu', 'searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
            }
        }
     return redirect('/');   
    }


    public function delete($lotid)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "cabActive";
        $setingsClass = "";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            {
                DB::statement("DELETE FROM `rates` WHERE `id` = '$lotid'");
                return view('auctioncabinet.bids', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
            }
        } 
        return redirect('/');
        } 
    
    public function auctioncabinet(Request $request)
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "cabActive";
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            return view('auctioncabinet', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        }
            return redirect('/home');
    }
    
    public function search()
    {
        $searchClass = "cabActive";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            return view('auctioncabinet.search', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        }
            return redirect('/home');    
    }
    
    public function catalog()
    {
        $searchClass = "";
        $catalogClass = "cabActive";
        $lotClass = "";
        $bidsClass = "";
        $setingsClass = "";
        
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
        return view('auctioncabinet.catalog', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        }
            return redirect('/home');
    }
    
    public function lot()
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "cabActive";
        $bidsClass = "";
        $setingsClass = "";
        
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            return view('auctioncabinet.lot', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
        
        }
            return redirect('/home');
    }
    
    public function bids()
    {
        $searchClass = "";
        $catalogClass = "";
        $lotClass = "";
        $bidsClass = "cabActive";
        $setingsClass = "";
        
        if (Auth::check())
        {
            $is_auch = $this->isAuctionHouse(Auth::id());
            if ($is_auch)
            return view('auctioncabinet.bids', compact('searchClass', 'catalogClass', 'lotClass', 'bidsClass', 'setingsClass'));
            
        }
            return redirect('/home');
        
    }
}
