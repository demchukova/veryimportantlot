<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuctionHouse extends Model
{
    protected $guarded = [];
    protected $table = 'auction_house';
}
